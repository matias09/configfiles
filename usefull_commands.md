g++ -fdump-tree-original  x.cpp

# --  Clang
# ---- Print code after preprocessing step
  clang++ -Xclang -ast-print  -fsyntax-only  -std=c++17 x.cpp

# --  Valgrind
# ---- Log to file memory checks given by valgrind
  valgrind -s --leak-check=full --track-origins=yes --log-file=memcheck.txt  ./a.out

#Bash
### Disable Cap Loc from the Machine
xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'

### No Annoying Blinking
gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$(gsettings get org.gnome.Terminal.ProfilesList default|tr -d \')/ cursor-blink-mode off


### Set Theme with gsettings on gnome. ( those are in /usr/share/themes )
gsettings set org.gnome.desktop.interface  gtk-theme  Adwaita-dark
gsettings set org.gnome.desktop.wm.preferences theme  Adwaita-dark


# --  How to cp with xargs help
ls -1 -rt | tail -17 | xargs  -I{} cp {} DEST

# --  Git Stuff
## --  How to view the grap of commits in a fancy way on command line
  git log --graph --oneline --all --decorate

## --  Shows file modifications from an older commit
  git show --patch-with-stat  [commit] [file]

## --  Set bck project saved in another directory for accept commits
  git config receive.denyCurrentBranch updateInstead

# --  How to send text to the clipboard
echo "saraza" | xclip -sel c

# --  How to hide mouse when its idle, Dependency : unclutter
unclutter -idle 0.01 -root &

# -- How to run woeusb-5.2.4.bash
## -- Previous steps, install wimlib
###   -- on Red Hat base distro is  wimlib-utils
###   -- on Debian base distro is  wimtools
  chmod +x woeusb-5.2.4.bash
  sudo ./woeusb-5.2.4.bash  --device .iso /dev/sdb --target-filesystem NTFS

# --  Utility to parallel bzip2 compression algorithm utility
# --    by default will compress, otherwise  add   -d   to decompress
pbzip2 -p [PROCESS\_NUMBER] -# <COMPRESSION\_QUALITY> <FILES>

# --  Tar Stuff
## --  Same as above, but for an entire folder
  tar cf [FILE\_NAME].tar.bz2 --use-compress-program=pbzip2 <DIRECTORY>

## --  How to use more than one processor to compress usin gzip algorithm
  tar -c [FOLDER\_TO\_COMPRESS] | pigz --best -p 10 > <FILE\_COMPRESSED>


# --  podman  Stuff
## --  How to select containers using regex
  podman ps --filter name=NAME\_HERE\* -aq | xargs podman stop | xargs podman rm

## --  If podman doesn't bring some package from a container, try this
  podman build -t [TAG\_NAME] podmanfile  --network=host

## --  podman backup image methods.
  podman save [img] > [file].tar
  podman load < [file].tar

## --  podman, load container, fixing COLUMNS and LINES
  podman exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -ti [CONTAINER] bash

## --  podman, how to show formatted list of containers
  podman container ls --format 'table {{.Names}}\t{{.Status}}'

## How to login to podman registry
  > podman login podman.io

## How to pull an image from podman registry
  > podman pull docker.io/wfameasurement/bazel

# Latex
## --  Command to send output to a folder and ran in safe mode
lualatex --safer --output-directory=FOLDER/ FILE.tex

## --  How to compile latex bibliography
> compile the tex file
> run bibtex looking for the  tex_filename.aux  where was compiled
    Eg. # bibtex ./cooked_files/texfile << in cooked_files there is already
                                           a texfile.aux created
> The bibtex result file has to be in the same directory where the tex file is.

> The key to make this work is run this sequence of commands:
  1. Build latex document
  2. Run Bibtex as was explained
  3. Build latex document once again
  4. Build latex document once again

> With each subsequent latex build, the references would be added.

### --- Shortcuts
  > Eg. run this command with the bibtex file on the same folder as the tex file.
  >> bibtex file = BIB_FILE.bib
      clear ; rm *.{log,aux,bbl,blg} ; lualatex --safer --output-directory=./cooked_files/ TEX_FILE.tex && bibtex ./cooked_files/TEX_FILE

  > Eg. run bibtex cmd inside of the where the TEX_FILE where build with the
          bibtex file in the same directory.
  >> bibtex file = BIB_FILE.bib
      clear ; rm *.{log,aux,bbl,blg} ; lualatex --safer --output-directory=./cooked_files/ TEX_FILE.tex && cd cooked_files && bibtex TEX_FILE

# --  how to format mysql cli query execution output as vertical per row
mysql --vertical=TRUE -p database < sql/a-queries.sql

" Output Example:
"
"   root@7a4d16d3322a:/sql# Enter password:
"    *************************** 1. row ***************************
"         NUM: 3234
"      nombre: cli-1
"   direccion: dir-1
"      ciudad: moron

# --  How to test the Microphone
arecord -t wav --max-file-time 30 mon.wav

# --  Tuxguitar
## --  Installation
  pacman install -y tuxguitar timidity++
## --  Run Timidity MIDI Port in background ( kill the process to stop it )
timidity -EFreverb=0 -iA -Os \>/dev/null & \</dev/null


## --  How to use Coredumps to jump on the last line executed
coredumpctl list -S $(date +%Y-%m-%d) | tail -1 | coredumpctl dump --output=core.dump

## --  Console diff command
  diff --suppress-common-lines --suppress-blank-empty -y -Z -E -w -B    FILE\_1 FILE\_2

# --  For exFAT Harddrives usage, [check fsck utility for fix partitions created]
pacman install  exfat-utils  fuse-exfat
## -- to avoid permission problems, mount hdd partitions with the next cmd
mount -o "uid=$UID,gid=$UID,umask=0000" /dev/[device\_nro] /mnt/[mount\_point]


# --  How commonly is use rsync for backups
## -- "--dry-run" list the operations but NOT executes then
  > #SRC= ; TRG= ; rsync -Curzv --dry-run --progress --log-file=$HOME/Documents/${SRC}\_${TRG}_$(date +%Y_%m_%d) --exclude="FOLDER_1" --exclude="FOLDER_2"  ${SRC} ${TRG}

## --  BEWARE Using this options will make an exact copy of the SRC into the TRG.  BEWARE
  > #SRC= ; TRG= ; rsync -Curzv --dry-run --delete --progress --log-file=$HOME/Documents/${SRC}_${TRG}_$(date +%Y_%m_%d) ${SRC} ${TRG}


# How to backup the entire linux system
  ># log_file="/tmp/rsync_log" ; SRC=test; TRG=/mnt/3m/fresh_install/linux_machines_snapshots/last_backup/; clear ; rsync --dry-run -Curzv --no-links --log-file=${log_file} --info=progress2 --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/usr/share/wallpapers/*","/lost+found","/home/*"} $SRC $TRG

# How to log each 2 sec how rsync is working
  > log_file="/tmp/rsync_log" ; while true ; do clear ; tail -5 ${log_file}; sleep 2; done

# Parameters to sync a folder which has hidden folders or files
  >rsync -avzP

### Arguments:
  -C auto-ignore files in the same way CVS does
  -a archive mode, equals -rlptgoD
  -v verbose
  -u update
  -z compress files before sent
  -b make backup
  -r recursive
  -p preserve permission, <- included in -a
  --no-links  avoid copy symlinks

# --  Quick almost commonly commands used with compressed files
  clear ; f=FILE;  echo "" && echo "---> UNPIGZ file ${f}.tar.gz" && unpigz -v -p 20 ${f}.tar.gz &&  echo "" && echo "---> UNTAR file ${f}.tar" && tar -xvf ${f}.tar && echo "" && echo "---> Erasing file ${f}.tar" && rm ${f}.tar

  #clear ; f=FILE|FOLDER ; echo "" && echo "---> TAR file ${f}" && tar -cvf ${f}.tar ${f} && echo "" && echo "---> PIGZ file ${f}" && pigz -v -p 20 ${f}.tar && echo "" && echo "---> Erasing folder ${f}" && rm ${f}/ -rf

  #for f in $(_FOLDER LIST_); do echo "" && echo "---> TAR file ${f}" && tar -cvf ${f}.tar ${f} && echo "" && echo "---> PIGZ file ${f}" && pigz -v -p 20 ${f}.tar && echo "" && echo "---> Erasing file ${f}" && rm ${f}/ -rf ; done

  -- Recommendation on how to list the folder $(ls -1 *.tar.gz | cut -d "." -f 1)
  #for f in $(ls -1 *.tar.gz | cut -d "." -f 1); do echo "" && echo "---> UNPIGZ file ${f}.tar.gz" && unpigz -v -p 20 ${f}.tar.gz &&  echo "" && echo "---> UNTAR file ${f}.tar" && tar -xvf ${f}.tar && echo "" && echo "---> Erasing file ${f}.tar" && rm ${f}.tar ; done

  #f=_TAR\_FILE\_NAME_ ; tar --list -f ${f}.tar.gz  >> ${f}\_info
  #f=_FOLDER_ ;clear ; ls -1 ${f} ; echo ; du -csh ${f} ; echo ; ls -1


  #clear; cd _FOLDER_ ; for f in $(ls  --hide=*.pdf --hide=*.chm --hide=*.exe --hide=*.zip --hide=*.msi --hide=*.torrent --hide=*.doc  --hide=*.txt) ; do f2=$(echo ${f} | cut -d "." -f 2) && mv ${f} ${f2} ; done ; cd -


# Example of how to mount automatically an external harddrive
# UUID=[UUID\_VALUE]   [mount point]    [filesystem type]	[options]  0 2
# UUID=323sdd-2323sdsd3sdxc   /mnt/toshiba    ext4	nofail,x-systemd.device-timeout=1ms  0 2

# How to install Alsa and dependencies needed
  > pacman -S alsa-plugins alsa-lib alsa-utils


# LVM
## How to create a lvm logical volume
  > lvcreate -L 30GB -n root_snapshot_2022_03_13 -s /dev/vg1/root

## How to apply changes from a SNAP LVM Volume
  > lvconvert --merge /dev/vg1/SNAP\_LV\_VOLUME

## How to a LVM Volume
  > lvremove VOLUME\_NAME/LV\_VOLUME\_NAME

# How to install KVM Virtualization Environment ( virt-manager )
## Install require packages
 > doas pacman -S dmidecode qemu vde2 dnsmasq bridge-utils openbsd-netcat

## Enable && Start libvirtd.service
  > systemctl enable libvirtd.service
  > systemctl start  libvirtd.service

## Add $(whoami) user
  > usermod -a -G libvirt $(whoami)
  > newgrp libvirt

## Fix "default" network not starting
  > doas virsh net-start default
  << if you run it without Priviledge, the net would be found >>

## !!! Important !!!
  > restart :)

# clang-tidy example
  clang-tidy src/main.cpp -checks=boost-*,bugprone-*,performance-*,readability-*,portability-*,modernize-*,clang-analyzer-cplusplus-*,clang-analyzer-*,cppcoreguidelines-*

f=FILE ; clang-format --sort-includes --style=Google ${f} >> tmp && echo  > ${f} && cat tmp > ${f} && rm tmp

# how to fix bluetoothctl
## list devices
  rfkill list all

## unblock bluetooth controller
  rfkill unblock <BLUETOOTH_ID>
  systemctl stop  bluetooth.service
  systemctl start bluetooth.service


# how to run mpv as we want :)
  mpv --cursor-autohide=always --no-osc -fs <FILE>

# how to set up/down brightness
  brightnessctl s 1

# how to use  sed  to remove unnecessary characters
  for f in $(ls -d1 *) ; do echo ${f} | sed -E "s/\.//" | xargs mv ${f} $1 ; done

# How to get the battery information
  upower -i /org/freedesktop/UPower/devices/battery_BAT0

# How to fix/change firefox / librewolf sound output from jack to alsa
  - In the browser address bar, go to "about:config"
  - Add a new config -> "media.cubeb.backend" , string, "alsa"
  - Reload, <<--- Enjoy

# Helper to select Seanson and Chapters in Series
  #lc=(01 02);s=01; for c in ${lc[@]} ; do mpv  -sid=0 --cursor-autohide=always --no-osc -fs  /PATH_TO_SERIE/s${s:1:1}/SERIE_${s}e${c}* ; done


# How to solve pacman error when try to update the system
## This issue is caused by a bad domain in the mirror list.
### Remove existing sync files
      rm -R /var/lib/pacman/sync/
### Edit your mirror list and move your country mirror to the top of the
###   list (You can use any text editor)
      nvim /etc/pacman.d/mirrorlist
### Now simply do
      pacman -Syu

# Brave browser on Arch with Wayland
## Download brave-bin from AUR
### tar -zxf brave-bin.tar.gz
### makepkg  PKGBUILD
### doas pacman -U  brave-bin-1:*.pkg.tar.zst

## Run it as :
> brave  --enable-features=UseOzonePlatform --ozone-platform=wayland

## Once opened,
### go to "brave:flags" in the URL bar
#### search for Change 'WebRTC PipeWire support'
#### set that to 'Enabled' in brave://flags
### search for "Preferred Ozone Platform"
##### set that to "Wayland"



# How to set 'ls cmd' folder and files colors using dirscolors
## dircolors  --print-database > .dir_colors
> eval $(dircolors /home/matt/.dir_colors)

# zsa wally deps:
> pacman -S libusb webkit2gtk gtk3

# How to send only files to a folder when in the same directory are folders
> n=12; folder=__single_files; for f in $(ls -1 | tail -n ${n}); do mv -v ${f} ${folder}; done

# Create and send university documents to it folder
> unit=00_; path=/home/$USER/Documents/personal_stuff/university/ ; mkdir ${path}/${unit} && mv *.* ${path}/${unit}


# How to add a string in front of a file name
> for f in {1..9}; do n=$(ls ${f}_*) ; f2=9_da_last_stunties_of_azul.webm ; mv 9_da_last_stunties_of_azul.webm 09_da_last_stunties_of_azul.webm ; done
> for f in {1..9}; do n=$(ls ${f}_*) ; f2=${n:0:-4} ;  mv ${n} 0${f2}.mp4 ; done

# Command to change "mpv" files adding date and nanoseconds at the end
> for f in FILES; do n=$(ls ${f}*) ; f2=${n:0:-4} ;  mv ${n} ${f2}_$(date +%Y_%m_%d__%N).jpg ; done


# How to download a torrent using transmission cli utility
> transmission-cli -D -U -v -w /home/${USER}/Downloads/ [TORRENT_FILE]

# How to allow a flatpak to write to the User Download directory
> flatpak override  --filesystem=xdg-download:create APP\_ID
df -h --type=fuseblk --type=ext4

# How to solve the libGl.so.1 missing lib
  apt-get update && apt-get install ffmpeg libsm6 libxext6  -y
