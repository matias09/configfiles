
filetype plugin on         " Enable filetype plugins
let mapleader=","

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Adds nerdtree config
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
source /home/$USER/Documents/configfiles/ides/text_based/nerdtree_config.vim

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Adds gitgutter config
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
source /home/$USER/Documents/configfiles/ides/text_based/gitgutter_config.vim

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Adds nerdcommenter config
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
source /home/$USER/Documents/configfiles/ides/text_based/nerdcommenter_config.vim

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Adds cpp enhanced syntax config
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
source /home/$USER/Documents/configfiles/ides/text_based/vim/cppenhancedsyntax_config.vim



if has('nvim')
  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  " => CoC Config
  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  source /home/$USER/Documents/configfiles/ides/text_based/nvim/coc_config.vim


  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  " => Plug 'jackguo380/vim-lsp-cxx-highlight' Conf
  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  source /home/$USER/Documents/configfiles/ides/text_based/nvim/vim-lsp-config.vim
endif




"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Adds u: config
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:C_UseTool_cmake = 'yes'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! CleanClose(tosave)
  let todelbufNr = bufnr("%")
  let newbufNr = bufnr("#")

  if ((newbufNr != -1) && (newbufNr != todelbufNr) && buflisted(newbufNr))
    exe "b".newbufNr
  else
    bnext
  endif

  if (bufnr("%") == todelbufNr)
    execute("NERDTree")
  endif

  exe "bd".todelbufNr
endfunction

" Delete trailing white space on save, useful for Python and CoffeeScript ;)
func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite *.* :call DeleteTrailingWS()

function! CmdLine(str)
    exe "menu Foo.Bar :" . a:str
    emenu Foo.Bar
    unmenu Foo
endfunction

function! VisualSelection(direction) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", '\\/.*$^~[]')
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'b'
        execute "normal ?" . l:pattern . "^M"
    elseif a:direction == 'gv'
        call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.')
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    elseif a:direction == 'f'
        execute "normal /" . l:pattern . "^M"
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction

" Don't close window, when deleting a buffer
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
   let l:currentBufNum = bufnr("%")
   let l:alternateBufNum = bufnr("#")

   if buflisted(l:alternateBufNum)
     buffer #
   else
     bnext
   endif

   if bufnr("%") == l:currentBufNum
     new
   endif

   if buflisted(l:currentBufNum)
     execute("bdelete! ".l:currentBufNum)
   endif
endfunction

function! GitStatus()
  let [a,m,r] = GitGutterGetHunkSummary()
  return printf('+%d ~%d -%d', a, m, r)
endfunction


function! SetTransparency()
  hi Normal  ctermbg=NONE cterm=NONE
  hi NonText ctermbg=NONE  cterm=none  ctermfg=0
  hi LineNr      ctermbg=NONE
  hi LineNrBelow ctermbg=NONE
  hi LineNrAbove ctermbg=NONE
endfunc

function! SetCustomUIColors()
  hi ColorColumn guibg=lightgrey ctermbg=0
  hi FoldColumn ctermbg=NONE cterm=NONE
  hi NonText ctermbg=NONE  cterm=none  ctermfg=0

  hi GitGutterAdd     guifg=NONE ctermfg=2 ctermbg=NONE
  hi GitGutterChange  guifg=NONE ctermfg=3 ctermbg=NONE
  hi GitGutterDelete  guifg=NONE ctermfg=1 ctermbg=NONE
  hi SignColumn       ctermbg=NONE cterm=NONE
endfunction

function! SetTransparencyAndUIColors()
  call SetTransparency()
  call SetCustomUIColors()
endfunc


" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Environment
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" highlight Cursor guifg=white guibg=black
" highlight iCursor guifg=white guibg=steelblue
" set guicursor=n-v-c:block-Cursor
" set guicursor+=i:ver100-iCursor
" set guicursor+=n-v-c:blinkon0
" set guicursor+=i:blinkwait10


" if &term =~ "xterm\\|rxvt"
"   " use an orange cursor in insert mode
"   let &t_SI = "\<Esc>]12;orange\x7"
"
"   let &t_EI = "\<Esc>]12;red\x7" " -------- -> -> -> use a red cursor otherwise
"   silent !echo -ne "\033]12;red\007"
"
"   " reset cursor when vim exits
"   autocmd VimLeave * silent !echo -ne "\033]112\007"
"
"   " use \003]12;gray\007 for gnome-terminal and rxvt up to version 9.21
" endif

" Technically this is for security
set exrc
set secure

" To save the rest of files in the buffer
set hidden


" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500


set t_Co=256               " Set 256 color
set history=700            " Sets how many lines of history VIM has to remember
set number                 " Set number line
set autoread               " Set to auto read when a file is changed from the outside
set ignorecase             " Ignore case when searching
set smartcase              " When searching try to be smart about cases
set hlsearch               " Highlight search results
set incsearch              " Makes search act like search in modern browsers
set lazyredraw             " Don't redraw while executing macros (good performance config)
set magic                  " For regular expressions turn magic on
set showmatch              " Show matching brackets when text indicator is over them
set mat=3                  " How many tenths of a second to blink when matching brackets
set splitbelow             " When we call another file with 'sp <file>' splits below
"set complete=.,w,b,u,t,i,kspell
set path+=**
set relativenumber
set encoding=utf-8
set splitright
set scrolloff=0
set signcolumn=yes          " Shift left bar some pixels
set cmdheight=2             " Give more space for displaying messages.


"set updatetime=300          " Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
                             " delays and poor user experience.


" au FileType c,cpp setlocal comments-=:// comments+=f://
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab, fold and indent related
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set expandtab               " Use spaces instead of tabs

" Be smart when using tabs ;)
set nosmarttab

" 1 tab == 2 spaces
set shiftwidth=2
set tabstop=2
set softtabstop=2

" Linebreak on 79 characters
" set textwidth=79
" set tw=79
" set lbr

set noai                     "Auto indent
set nosi                     "Smart indent
set wrap                    "Wrap lines

" Set fold methods with identation
"set fdm=syntax
" set foldmethod=syntax

" improve autocomplete
"set omnifunc=syntaxcomplete#Complete
"set nocp " required for omnicppcomplete



"This will make the syntax synchronization start 100 lines before the first
"displayed line.  The default value is 50 (15 when c_no_if0 is set).  The
"disadvantage of using a larger number is that redrawing can become slow.
let c_minlines = 15

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable               " Enable syntax highlighting
" if has('termguicolors') && $DISPLAY !=? ''
"     set termguicolors
" endif

"" set cursorline
set colorcolumn=80

colorscheme seoul256
" colorscheme distinguished


" call SetCustomUIColors()
call SetTransparencyAndUIColors()

""""""""""""""""""""""""""""""
" => Status line
" """"""""""""""""""""""""""""""
  set laststatus=2            " Always show the status line

  " Format the status line
  "set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l

  set statusline=
  set statusline+=%#PmenuSel#
  set statusline+=%#LineNr#
  set statusline+=\ %f
  set statusline+=%m\
  set statusline+=%=
  set statusline+=%#CursorColumn#
  set statusline+=\ %y
  set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
  set statusline+=\[%{&fileformat}\]
  set statusline+=\ %p%%
  set statusline+=\ %l:%c
  set statusline+=\
" --------------------------------------------------
"  End Set Status Bar
" --------------------------------------------------


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => ?
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set modeline
set relativenumber          " enable relative line numbers
set confirm                 " ask confirmation for some things, like save before quit, etc.
set wildmenu                " Tab completion menu when using command mode
set shortmess+=aAcIws       " Hide or shorten certain messages
set clipboard^=unnamedplus   " system clipboard (requires +clipboard)

" Reload changes if file changed outside of vim requires autoread
augroup load_changed_file
    autocmd!
    autocmd FocusGained,BufEnter * if mode() !=? 'c' | checktime | endif
    autocmd FileChangedShellPost * echo "Changes loaded from file"
augroup END

" when quitting a file, save the cursor position
augroup save_cursor_position
    autocmd!
    autocmd BufReadPost * call setpos(".", getpos("'\""))
augroup END


"------------------------- Mapping keys --------------------------------------
nnoremap <leader>R  :source $HOME/Documents/configfiles/ides/text_based/cross_config.vim
nnoremap <leader>ee i ⏎

noremap <leader>su  :call SetCustomUIColors()<CR>
noremap <leader>st  :call SetTransparency()<CR>
noremap <leader>stu :call SetTransparencyAndUIColors()<CR>
noremap <leader>ds  :call DeleteTrailingWS()<CR>

" nnoremap fo ggVGzogg
nmap V  00v$h
nmap <leader>uu gUaw

" close pane
nnoremap cp :q<CR>

" files buffer navigation
nnoremap T <C-^>
nnoremap <leader>n :bn<CR>
nnoremap <leader>p :bp<CR>
map fc <Esc>:call CleanClose(1)<CR>
map fq <Esc>:call CleanClose(0)<CR>

" list open buffers to select
noremap <leader>lf  :ls<CR>:b

map <RETURN> ""

" resize panes
noremap <Leader><C-l>  :vertical resize +3<CR>
noremap <Leader><C-h>  :vertical resize -3<CR>
noremap <Leader><C-j>  :resize -3<CR>
noremap <Leader><C-k>  :resize +3<CR>

" Horizontal
nnoremap <Leader>hr  :resize
nnoremap <Leader>hra :resize +15<CR>
nnoremap <Leader>hrr :resize -15<CR>

" Vertical
nnoremap <Leader>vr  :vertical resize
nnoremap <Leader>vra :vertical resize +9<CR>
nnoremap <Leader>vrr :vertical resize -9<CR>

nnoremap <Leader>ss  i<C-r>=
nnoremap <leader>hh  :noh<CR>

" movements
noremap <C-j>  <C-w><C-j>
noremap <C-k>  <C-w><C-k>
noremap <C-h>  <C-w><C-h>
noremap <C-l>  <C-w><C-l>


" Move a line of text using CTRL+[mn] and SHIFT+[mn]
" nmap <C-m> :m-2<cr>
" nmap <C-n> :m+<cr>
" vmap M :m'<-2<cr>`>my`<mzgv`yo`z
" vmap N :m'>+<cr>`<my`>mzgv`yo`z

" noremap <F10> :Explore<CR><C-w>h:vertical resize +15<CR>

" disable CTR-]
" noremap <C-]> <NOP>

map K ""
