
let g:ycm_always_populate_location_list = 1

let g:ycm_enable_diagnostic_signs = 1
let g:ycm_enable_diagnostic_highlighting = 0 " default 1
let g:ycm_echo_current_diagnostic = 1

let g:ycm_error_symbol = 'Ⅹ'
let g:ycm_warning_symbol = '△' 

let g:ycm_open_loclist_on_ycm_diags = 1 " default 1
let g:ycm_add_preview_to_completeopt = 1 " default 0

let g:ycm_autoclose_preview_window_after_completion = 1 " default 0
let g:ycm_auto_hover = 0 " default 1

let g:ycp_key_list_select_completion = ['<TAB>', '<Down>', '<Enter>']
let g:ycp_key_list_previous_completion = ['<S-TAB>', '<Up>']
let g:ycp_key_list_stop_completion = ['<C-y>', '<Escape>']
let g:ycp_key_detailed_diagnostics = '<leader>d' " default <leader>d

let g:ycm_use_ultisnips_completer = 0 " default 1

" highlight YcmErrorLine guibg=#0000ff
" highlight YcmWarningLine guibg=#00ff00

nmap <leader>D <plug>(YCMHover)
nmap <leader>yfw <plug>(YCMFindSymbolInWorkspace)
nmap <leader>yfd <plug>(YCMFindSymbolInDocument)
