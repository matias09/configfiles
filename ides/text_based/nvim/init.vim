set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Adds nerdtree config between vim and nvim
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
source /home/$USER/Documents/configfiles/ides/text_based/cross_config.vim


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Adds External Functionality using Plugin Manager ( Plug )
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
source /home/$USER/Documents/configfiles/ides/text_based/nvim/plugins.vim
