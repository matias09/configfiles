" Sets always the changes vertical Bar
set signcolumn=yes

" Vertical Line
let g:gitgutter_sign_allow_clobber = 0
let g:gitgutter_set_sign_backgrounds = 0

" Symbols
let g:gitgutter_sign_added = '+'
let g:gitgutter_sign_modified = '~'
let g:gitgutter_sign_removed = '-'
let g:gitgutter_sign_removed_first_line = '^'  " from beggining '^^'
let g:gitgutter_sign_modified_removed = 'w'    " from beggining 'ww'

" Line highlights
" GitGutterAddLine          " default: links to DiffAdd
" GitGutterChangeLine       " default: links to DiffChange
" GitGutterDeleteLine       " default: links to DiffDelete
" GitGutterChangeDeleteLine " default: links to GitGutterChangeLineDefault, i.e. DiffChange

highlight link GitGutterChangeLine DiffText

" Line numbers highlights
" GitGutterAddLineNr          " default: links to CursorLineNr
" GitGutterChangeLineNr       " default: links to CursorLineNr
" GitGutterDeleteLineNr       " default: links to CursorLineNr
" GitGutterChangeDeleteLineNr " default: links to CursorLineNr

highlight link GitGutterChangeLineNr Underlined

nnoremap <Leader>ggf  :GitGutterFold<CR>
nnoremap <Leader>ggp  :GitGutterPreviewHunk<CR><C-w>j

nnoremap <Leader>ggu  :GitGutterUndoHunk<CR>
nnoremap <Leader>ggs  :GitGutterStageHunk<CR>
