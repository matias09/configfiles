autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

map <F12> :NERDTreeFind<CR>:vertical resize 50<CR>
map <F11> :NERDTreeToggle<CR>:vertical resize 50<CR>

let g:NERDTreeDirArrows = 1
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

" Nerd Comment Options
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Bookmarks by default
let NERDTreeShowBookmarks=0

" Align line-wise comment delimiters flush left
" instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Open nerd Tree on the right side
let g:NERDTreeWinPos = "right"
