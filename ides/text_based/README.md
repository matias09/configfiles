# NVim
## Plugin Manager
  > Create path 
      /home/$USER/.local/share/
  > Copy folder into previous path
      /mnt/toshiba/personal_stuff/app/ides/text_based/nvim/plugin_manager/nvim/

## Internal Conf
  > Create path 
      /home/$USER/.config/nvim/
  > Create hard link file into previous path
      /home/$USER/configfiles/ides/text_based/nvim/init.vim

  > Create path 
      /home/$USER/.vim/
  > Unzip plugins files into previous path to added them
      E.g. /home/$USER/configfiles/ides/text_based/vim/plugins/nerd_tree.zip

# Coc 
## Node and NPM
  > Create path
      /home/$USER/Documents/development/tools/
  > Copy and Decompress file into previous path
      /mnt/toshiba/development/tools/node-v14.15.4-linux-x64.tar.xz 
        -- tar -xvf [FILE]
  > Create link file for binaries in bin folder
      ln -sf [NODE_PATH]/bin/node /usr/bin/node
      ln -sf [NODE_PATH]/bin/npm /usr/bin/npm

  > Create configuration hard link
      ln /home/$USER/configfiles/ides/text_based/nvim/coc-settings.json
      $HOME/.config/nvim/

 
## Added plugins
  :CocInstall  coc-json coc-clangd
  :CocCommand clangd.install

## CCLS
  > Install package "ccls" from distro repository
  > Add :
    "/home/$USER/configfiles/ides/text_based/nvim/dot_ccls_per_proj_conf"
        to the root of each project.

__Another option is :__

## Cmake Conf
  The last part of the ceremony involves using CMake. 
  All we need is, adding a single line definition on toplevel CMakeLists.txt:

  set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

  This generates a file called compile_commands.json on build directory. 
  It contains include paths, compiler command and options. 
  These helps clangd to figure out what is where.

  If the project don't use CMake, but make, 
  you can use Bear ( sudo <package mgr> install bear ) 
  to generate this file with bear make command.


# Final Thoughts
  You have to test both options
