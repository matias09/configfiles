#----------------------------------------------------------#
#-------------------- Config ------------------------------#
#----------------------------------------------------------#

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

MATT_CONFIG_FILES="/home/$USER/Documents/configfiles"
MATT_SCRIPTS_PATH="${MATT_CONFIG_FILES}/.matt_scripts"

#-------------------- PS1 Methods --------------------------#
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
#---------------- END PS1 Methods --------------------------#

#-----------------------
# Shop
#-----------------------
shopt -s histappend # append to the history file, don't overwrite it
shopt -s checkwinsize # check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s expand_aliases # expand aliases

#-----------------------
# Export
#-----------------------
export XDG_CONFIG_DIR="/home/${USER}/.config"

# Priorities wayland above X11
# export SDL_VIDEODRIVER="wayland,x11"

export HISTCONTROL=ignoreboth:ignoredups:erasedups  # no duplicate entries
export HISTSIZE=100000                              # big big history
export HISTFILESIZE=100000                          # big big history

export EDITOR=nvim

export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND" # Save and reload the history after each command finishes
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

#PS1='[\u@\h \W]\$ '
#PS1="\[\e[01;36m\]\t\[\e[02;33m\]\$(parse_git_branch)\[\033[00m\]\[\e[01;34m\] \W \[\e[01;32m\]-> \[\e[0m\]"
export PS1="\[\e[02;33m\]\$(parse_git_branch)\[\033[00m\]\[\e[01;34m\] \W \[\e[01;32m\]-> \[\e[0m\]"
export LS_COLORS=$LS_COLORS:'di=1;38:ln=36' ; export LS_COLORS;

export VULKAN_SDK_PATH=/home/matt/vulkan/1.1.92.1/x86_64
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig/

export PATH="$PATH:$HOME/.local/bin:$HOME/bin:/usr/local/go/bin"

export GOPATH=$HOME/gitprojects/go

# gtk theme
# export QT_QPA_PLATFORMTHEME='gtk3'
# export GTK_THEME=Adwaita-dark

# export CALIBRE_USE_DARK_PALETTE=1

#-----------------------
# Settings
#-----------------------
set -o vi

ulimit -c unlimited # Enabling C++ Core Dump Files Generation
