
#--------------------------------------------------------------#
#-------------------- Functions -------------------------------#
#--------------------------------------------------------------#
tss() {
  tmux_exe="tmux -2 "

  parameter_sel=$1
  if [[ ${parameter_sel} != "" ]]; then
    ${tmux_exe} new-session -t ${parameter_sel}
    return
  fi

  current_sessions="$(tmux list-sessions| sed -e 's/\(^[a-z_]*\).*/\1/')"
  user_selection=''

  echo "-------------------------------------------------------------"
  echo "-------------------- SESSIONS -------------------------------"
  echo "-------------------------------------------------------------"
  echo ${current_sessions}
  echo
  echo " Select :"
  echo -n ">>> "
  read user_selection

  ${tmux_exe} new-session -t ${user_selection}
}

load_tmux() {
  tmux_exe="tmux -2 "
  ${tmux_exe} attach-session -t normal_stuff
}

uenvironmentview() {
  [[ ! -v "XDG_SESSION_TYPE" ]] && echo "ENV variable XDG_SESSION_TYPE not set" \
                                && echo "Bye ..." \
                                && return 1

  [[ ! -v "1" ]] && echo "Kanshi conf parameter empty" \
                 && echo "Bye ..." \
                 && return 1

  ENV_VIEW_SELECTED=${1}

  deps="swaybg kanshi"
  for p in ${deps}; do
    e=$(whereis ${p})
    r=${e/${p}:/}
    [[ ! -v "r" ]] && echo "${p} is Not Installed" \
                   && echo "Bye ..." \
                   && return 1
  done

  MATT_CONFIG_FILES="/home/$USER/Documents/configfiles"
  MATT_SCRIPTS_PATH="${MATT_CONFIG_FILES}/.matt_scripts"
  KANSHI_PROFILES_PATH="${XDG_CONFIG_DIR}/kanshi"
  NEXT_MON_ORIENTATION=${ENV_VIEW_SELECTED}
  # NEXT_MON_ORIENTATION=${ENV_VIEW_SELECTED@U} # 'str@U' turn to uppercase

  KANSHI_CURRENT_CONF_SET=$(ls -1 ${KANSHI_PROFILES_PATH})

  conf_found=0
  for c in ${KANSHI_CURRENT_CONF_SET}; do
    if [[ ${NEXT_MON_ORIENTATION} == ${c} ]]; then
      conf_found=1
      break
    fi
  done

  [[ ${conf_found} -eq 0 ]] &&  echo "Kanshi conf Not Expected..." \
                            &&  return 1

  kanshi -c ${KANSHI_PROFILES_PATH}/${NEXT_MON_ORIENTATION} &
  bash ${MATT_SCRIPTS_PATH}/set_wallpapers.sh
}
