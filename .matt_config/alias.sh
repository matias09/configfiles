
#----------------------------------------------------------#
#-------------------- Alias -------------------------------#
#----------------------------------------------------------#

#list
alias ls='ls --color=auto --group-directories-first'
alias lr='ls -ar'
alias lrt='ls -latr'
alias l.="ls -A | egrep '^\.'"

#free
alias free="free -mth"

#use all cores
alias uac="sh ~/.bin/main/000*"

#userlist
alias userlist="cut -d: -f1 /etc/passwd"

#ps
alias ps="ps auxf"
alias psgrep="ps auxh | grep -v grep | grep -i -e VSZ -e"

#grub update
alias update-grub="doas grub-mkconfig -o /boot/grub/grub.cfg"

#improve png
alias fixpng="find . -type f -name "*.png" -exec convert {} -strip {} \;"

#add new fonts
alias fc='doas fc-cache -fv'

#hardware info --short
alias hw="hwinfo --short"

#check vulnerabilities microcode
alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'

#mounting the folder Public for exchange between host and guest on virtualbox
alias vbm="doas mount -t vboxsf -o rw,uid=1000,gid=1000 Public /home/$USER/Public"

#youtube-dl
alias yta-best="yt-dlp --extract-audio --audio-format best "

alias yta-aac="yt-dlp --extract-audio --audio-format aac "
alias yta-flac="yt-dlp --extract-audio --audio-format flac "
alias yta-m4a="yt-dlp --extract-audio --audio-format m4a "
alias yta-mp3="yt-dlp --extract-audio --audio-format mp3 "

alias ytv-best="yt-dlp -f bestvideo+bestaudio "

#git
alias gs="clear ; git status --short "
alias gb="clear ; git branch -a "
alias gr="git remote -v "
alias gl="clear ; git log -n $4 "
alias gls="clear ; git log --stat -n $5"

#podman
alias pdc="clear  ; podman container ls"
alias pdca="clear ; podman container ls -a"
alias pdn="clear  ; podman network ls"
alias pdi="clear  ; podman image ls"
alias pdv="clear  ; podman volume ls"
alias pdp="clear  ; podman ps"
alias pds="clear  ; podman ps -a"

#docker compose
alias dcr="docker-compose run "

#vagrant
alias vgt="vagrant "

#various
#get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"

alias cs='clear'
alias cd='cd ; cs'

alias rsl='clear ; ls -1'
alias rsla='clear ; ls -la'
alias aes='uenvironmentview 1v_10v_1h'

alias tuxguitar='timidity -EFreverb=0 -iA -Os \>/dev/null & \</dev/null & tuxguitar & echo "kill timidity"'

alias w3m="w3m -cols 80 "

alias lynx="lynx -cfg=/home/$USER/lynx.cfg -lss=/home/$USER/lynx.lss"

alias sar="python3 ${MATT_SCRIPTS_PATH}/search_and_replace.py -s '.| |-|[|]|{|}|(|)' -r '_' -d '|'"
alias sar_empty_search="python3 ${MATT_SCRIPTS_PATH}/search_and_replace.py"

alias grep="grep -n  --color=auto | grep -v grep"

alias lt="load_tmux"

alias resett="reset && tmux clear-history"

alias 3py="python3 "
alias 3lite="sqlite3 "

alias mupdf="mupdf -I -C 777777 "

#get fastest mirrors in your neighborhood
alias mirror="doas reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="doas reflector --latest 30 --number 10 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="doas reflector --latest 30 --number 10 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="doas reflector --latest 30 --number 10 --sort age --save /etc/pacman.d/mirrorlist"
#our experimental - best option for the moment
alias mirrorx="doas reflector --age 6 --latest 20  --fastest 20 --threads 5 --sort rate --protocol https --save /etc/pacman.d/mirrorlist"
alias mirrorxx="doas reflector --age 6 --latest 20  --fastest 20 --threads 20 --sort rate --protocol https --save /etc/pacman.d/mirrorlist"
alias ram="rate-mirrors --allow-root arch | doas tee /etc/pacman.d/mirrorlist"

alias getccl="ln ${MATT_CONFIG_FILES}/ides/text_based/nvim/dot_ccls_per_proj_conf.ccls"
alias updatecalcursedata="calcurse  -P ; calcurse -i  $HOME/calcurse.ical ; echo 'Refresh calcurse window Shift+r '"

# Local
alias cdbooks="cd /home/$USER/Documents/library/books/"
alias cdcourses="cd /home/$USER/Documents/library/courses/"
alias cdpersonalstuff="cd /home/$USER/Documents/personal_stuff/"
alias cddevelopment="cd /home/$USER/Documents/development/"
alias cdconfigfiles="cd /home/$USER/Documents/configfiles/"
alias cdgitprojects="cd /home/$USER/Documents/development/gitprojects/"
alias cdtestshitcode="cd /home/$USER/Documents/development/test_shit_code/"

# ext harddrive
alias cdextbooks="cd /mnt/3m/library/books/"
alias cdextcourses="cd /mnt/3m/library/courses/"
alias cdextpersonalstuff="cd /mnt/3m/personal_stuff/"
alias cdextdevelopment="cd /mnt/3m/development/"
alias cdextconfigfiles="cd /mnt/3m/configfiles/"
alias cdextgitprojects="cd /mnt/3m/development/gitprojects/"
alias cdexttestshitcode="cd /mnt/3m/development/test_shit_code/"

alias bckpersonalstuff="bash ${MATT_SCRIPTS_PATH}/backups/personal_stuff.sh"
alias bckconfiguration="bash ${MATT_SCRIPTS_PATH}/backups/programs_config.sh"
alias processmpvscreenshots="bash ${MATT_SCRIPTS_PATH}/process_mpv_horizontal_screenshots.sh"

# Get path of folders --- NOT Working at the moment
# alias getpath_books="f1=/home/$USER/Documents/library/books/ && cd ${f1} && \$(pwd | wl-copy)  && echo \"Path ${f1} got it . . . \" && cd -"
# alias getpath_courses="f2=/home/$USER/Documents/library/courses/ && cd ${f2} && \$(pwd | wl-copy) && echo \"Path ${f2} got it . . . \" && cd -"
# alias getpath_personalstuff="f3=/home/$USER/Documents/personal_stuff/ && cd ${f3} && \$(pwd | wl-copy) && echo \"Path ${f3} got it . . . \" && cd -"
# alias getpath_development="f=/home/$USER/Documents/development/ && cd ${f} && \$(pwd | wl-copy) && echo \"Path ${f} got it . . . \" && cd -"
# alias getpath_configfiles="f=/home/$USER/Documents/configfiles/ && cd ${f} && \$(pwd | wl-copy) && echo \"Path ${f} got it . . . \" && cd -"
# alias getpath_gitprojects="f=/home/$USER/Documents/development/gitprojects/ && cd ${f} && \$(pwd | wl-copy) && echo \"Path ${f} got it . . . \" && cd -"
# alias getpath_testshitcode="f=/home/$USER/Documents/development/test_shit_code/ && cd ${f} && \$(pwd | wl-copy) && echo \"Path ${f} got it . . . \" && cd -"
#
# alias getpath_cdextbooks="f=/mnt/3m/library/books/ && cd ${f} && \$(pwd | wl-copy) && echo \"Path ${f} got it . . . \" && cd -"
# alias getpath_cdextcourses="f=/mnt/3m/library/courses/ && cd ${f} && \$(pwd | wl-copy) && echo \"Path ${f} got it . . . \" && cd -"
# alias getpath_cdextpersonalstuff="f=/mnt/3m/personal_stuff/ && cd ${f} && \$(pwd | wl-copy) && echo \"Path ${f} got it . . . \" && cd -"
# alias getpath_cdextdevelopment="f=/mnt/3m/development/ && cd ${f} && \$(pwd | wl-copy) && echo \"Path ${f} got it . . . \" && cd -"
# alias getpath_cdextconfigfiles="f=/mnt/3m/configfiles/ && cd ${f} && \$(pwd | wl-copy) && echo \"Path ${f} got it . . . \" && cd -"
# alias getpath_cdextgitprojects="f=/mnt/3m/development/gitprojects/ && cd ${f} && \$(pwd | wl-copy) && echo \"Path ${f} got it . . . \" && cd -"
# alias getpath_cdexttestshitcode="f=/mnt/3m/development/test_shit_code/ && cd ${f} && \$(pwd | wl-copy) && echo \"Path ${f} got it . . . \" && cd -"



# -- Only for Wayland Window Server --
alias pwdc="pwd | wl-copy"
alias setwall="bash ${MATT_SCRIPTS_PATH}/desktop_environment/set_wallpapers.sh"


#--------------------------------------------------------------#
#-------------------- auto_run_cmd ----------------------------#
#--------------------------------------------------------------#

eval $(dircolors /home/matt/.dir_colors)
