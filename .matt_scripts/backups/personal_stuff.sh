#!/bin/env bash

PRG="rsync"
ARGS=" -Curzv --no-links --info=progress2 "

EXEC="${PRG} ${ARGS}"

MNT_DIR="/mnt"
EXTERNAL_HARD_DRIVES_FOLDERS="3m bck_concerts bck_library_1 bck_library_2 bck_music bck_toshiba library mby3 miii mmm movies music_concerts_cinematics_shows mym new_hd_1_1 new_hd_1_2 new_hd_2_1 new_hd_2_2 pen_black pen_blue pen_gris series tm toshiba vancouver"

SCRIPT_NAME="$(basename $0)"        # gets filename
SCRIPT_NAME="${SCRIPT_NAME:0:-3}"   # remove last 3 chars

DOCUMENTS_PATH="/home/$USER/Documents"
LOG_FILE="${DOCUMENTS_PATH}/log_${SCRIPT_NAME}_$(date +%Y_%m_%d).log"

echo
echo "---- Looking for backup Harddrive ----"
harddrive_data_source=""
for f in ${EXTERNAL_HARD_DRIVES_FOLDERS} ; do
  tmp_dir="${MNT_DIR}/${f}/environment"
  if [[ -d ${tmp_dir} ]] ; then
    harddrive_data_source="${MNT_DIR}/${f}"
    break
  fi
done

if [[ ! -d ${harddrive_data_source} ]] ; then
  echo $'\t' "-- Environment confg files, not found." $'\n' \
       $'\t' "-- External disk not mounted."
  exit 1
fi

[[ ! -v "WALLPAPERS_FOLDER" ]] && echo "ENV variable WALLPAPERS_FOLDER not set" \
                               && echo "Bye ..." \
                               && exit 1

$EXEC --delete ${DOCUMENTS_PATH}/personal_stuff/ ${harddrive_data_source}/personal_stuff/ > ${LOG_FILE}
$EXEC --delete ${WALLPAPERS_FOLDER}/ ${harddrive_data_source}/wallpapers/ >> ${LOG_FILE}
