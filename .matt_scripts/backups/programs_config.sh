#!/bin/env bash

PRG="rsync"
ARGS=" -Curzv --no-links --info=progress2 "

EXEC="${PRG} ${ARGS}"

MNT_DIR="/mnt"
EXTERNAL_HARD_DRIVES_FOLDERS="3m bck_concerts bck_library_1 bck_library_2 bck_music bck_toshiba library mby3 miii mmm movies music_concerts_cinematics_shows mym new_hd_1_1 new_hd_1_2 new_hd_2_1 new_hd_2_2 pen_black pen_blue pen_gris series tm toshiba vancouver"

SCRIPT_NAME="$(basename $0)"        # gets filename
SCRIPT_NAME="${SCRIPT_NAME:0:-3}"   # remove last 3 chars

DOCUMENTS_PATH="/home/$USER/Documents"
LOG_FILE="${DOCUMENTS_PATH}/log_${SCRIPT_NAME}_$(date +%Y_%m_%d).log"

echo
echo "---- Looking for backup Harddrive ----"
harddrive_data_source=""
for f in ${EXTERNAL_HARD_DRIVES_FOLDERS} ; do
  tmp_dir="${MNT_DIR}/${f}/environment"
  if [[ -d ${tmp_dir} ]]; then
    harddrive_data_source="${MNT_DIR}/${f}"
    break
  fi
done

if [[ ! -d ${harddrive_data_source} ]] ; then
  echo $'\t' "-- Environment confg files, not found." $'\n' \
       $'\t' "-- External disk not mounted."
  return 1
fi

dot_config="neomutt foot kanshi"
kanshi_dir="/usr/share/custom_scripts"

echo "---- Backup $HOME/.config/ Folders  ----"
for f in ${dot_config}; do
  if [[ -d $HOME/.config/${f} ]]; then
    $EXEC $HOME/.config/${f} ${DOCUMENTS_PATH}/configfiles/dot_config/ > ${LOG_FILE}
  elif [[ ${f} == "kanshi" ]] && [[ -d ${kanshi_dir}/${f} ]]; then
    $EXEC ${kanshi_dir}/${f} ${DOCUMENTS_PATH}/configfiles/dot_config/ > ${LOG_FILE}
  fi
done

echo "---- backup /etc/ly  ----"
$EXEC /etc/ly ${harddrive_data_source}/environment/session_loaders >> ${LOG_FILE}

echo "---- backup configfiles  ----"
$EXEC --delete ${DOCUMENTS_PATH}/configfiles/  ${harddrive_data_source}/environment/configfiles/ >> ${LOG_FILE}
