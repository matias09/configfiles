#!/usr/bin/env bash

DIR="/home/$USER/Documents/library/books"

if [[ ! -d ${DIR} ]]; then
  echo "--MSG: '/mnt/tm/books' folder NOT FOUND"
  exit 0
fi

SUBJECT=$(ls ${DIR}/ | dmenu -l 50 -i)

if [[ ${SUBJECT} == "" ]]; then
  echo "--MSG: Book Category NOT Selected"
  exit 0
fi

SELECTION=$(ls ${DIR}/${SUBJECT}/ | dmenu -l 50 -i)
SELECTION_PATH="${DIR}/${SUBJECT}/${SELECTION}"

FILE_EXTENSION=$(ls ${SELECTION_PATH} | cut -d . -f 2)

if [[ ${FILE_EXTENSION} == "" ]]; then
  exit 0
fi

prg=""
if [[ $FILE_EXTENSION == "chm" ]]; then
  prg="xchm"
else
  prg="zathura"
fi

${prg} ${SELECTION_PATH} &
