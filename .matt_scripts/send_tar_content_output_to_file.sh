#!/usr/bin/env bash

tar_gz_file_content=$1

if [[ -z $tar_gz_file_content ]]; then
  echo "-- Error: File where redirect output, was not given"
  echo "-- Expected: ./run <FILE>"
  exit
fi

files=$(ls *.tar.gz)

echo "-- BEGIN"

for f in $files; do
  echo $f
  echo "--------------------------------------------" >> $tar_gz_file_content
  echo "--------------------------------------------" >> $tar_gz_file_content
  tar -t -f $f >> $tar_gz_file_content
  echo "--------------------------------------------" >> $tar_gz_file_content
  echo "--------------------------------------------" >> $tar_gz_file_content
done

echo "-- END"
