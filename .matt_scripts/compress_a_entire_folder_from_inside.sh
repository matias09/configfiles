#!/bin/bash

RED=`tput setaf 1`
GRE=`tput setaf 2`
MAG=`tput setaf 3`
NC=`tput sgr0` # No Color

echo "${MAG}Start Compressing File . . .${NC}"

for d in *;  do
  if [[ -d $d ]]; then
    f=$d".tar.gz"
    if !([[ -f $f ]]); then
     echo " ${MAG}-------  Compress file : $d.tar.gz -------------------------------${NC}";  echo ""
     tar -vc $d | pigz -9 -p 15 > $f

      if [ $? -eq 0 ]; then
        echo " ------- [${GRE}SUCCESS${NC}]: Compressed file : $f -- [${GRE}Ok${NC}]"
      else
        echo " ------- [${RED}ERROR${NC}]: Compressed file : $f --  [${RED}Fail${NC}]"
      fi

      echo ""
    fi
  fi
done

echo "${MAG}End . . .${NC}"
