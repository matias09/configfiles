#!/usr/bin/python3
import os, argparse

CHAR_UNDERSCORE = '_'
CHAR_POINT = '.'

BACK_ONE_DIR = "../"

def changeChar(s, p, r):
    return s[:p]+r+s[p+1:]


def removeContiguousChars(string, text_to_replace) :
    str_pieces = string.split(text_to_replace)

    string = ""

    for string_piece in str_pieces :
        if (string_piece != "") :
            string += string_piece + text_to_replace

    return string[0:len(string) - 1]

def searchAndReplace(string, chars_to_search, text_to_replace) :
    last_dot_position = string.rfind(CHAR_POINT)

    for char in chars_to_search :
        if (char in string) :
            string = string.replace(char, text_to_replace)

    if (os.path.isdir(string) == False) :
        string = changeChar(string, last_dot_position, CHAR_POINT)

    return string


def processDirectory(path, chars_to_search, text_to_replace) :
   os.chdir(path)
   content = os.listdir(CHAR_POINT)

   for file_or_dir_name in content :
       tmp_file_or_dir_name = file_or_dir_name

       if (os.path.isdir(file_or_dir_name) == True) :
           processDirectory(file_or_dir_name, chars_to_search,
               text_to_replace)

       #  if (file_or_dir_name.find(CHAR_POINT) != -1) :
       file_or_dir_name = searchAndReplace(file_or_dir_name, chars_to_search,
               text_to_replace)
       file_or_dir_name = removeContiguousChars(file_or_dir_name,
               text_to_replace)
       os.rename(tmp_file_or_dir_name, file_or_dir_name.lower())

   os.chdir(BACK_ONE_DIR)
   return True


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s','--search-string',
            help='Char/s to find',
            required=True)

    parser.add_argument('-r','--replace-string',
            help='Char for replace',
            required=True)

    parser.add_argument('-d','--delimiter',
            help='Separator of each char for replace.',
            required=False)

    parser.add_argument('-p','--path-to-search',
            help='Folder where the script will begin to run.',
            required=False)

    args = parser.parse_args()

    # if we are looking for more than one char and we not
    # specify a char delimiter, we will going to find
    # for all that consecutive chars.
    if (len(args.search_string) > 1 and len(args.delimiter) == 0) :
        print("-- INFO -- main()")
        print("\t -- Error: Delimiter not specified.")
        exit()

    # if path is not set, we set the current directory
    if (args.path_to_search) :
        path = args.path_to_search
    else :
        path = CHAR_POINT

    # call to main search and replace function
    processDirectory(path, args.search_string.split(args.delimiter),
            args.replace_string)
    return True
# --------------------------------------

if __name__ == "__main__":
    main()

