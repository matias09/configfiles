#!/usr/bin/env bash

# normal_stuff session #
srv="normal_stuff"

# start new session #
 new-session -d -s $srv

# rename the first window and start a terminal #
 rename-window -t 0 'surfing'

 send-keys -t  'surfing'            \
               'clear' C-m


# add a new windows #
# multimedia
 new-window     -t $srv:2 -n 'multimedia'

 send-keys     -t  'multimedia'    \
                   'cmus' C-m
#  splitp
#  send-keys     -t  'multimedia'    \
#                    'alsamixer' C-m
 splitp
 resize-pane -D 300 # just a big number

 send-keys     -t  'multimedia'    \
                   'bluetoothctl' C-m


# vifm
 # new-window    -t $srv:7 -n 'file_explorer'
 # send-keys     -t  'file_explorer'  \ 'vifm' C-m

# w3m
 new-window    -t $srv:3 -n 'research'
 send-keys     -t  'research'  \ 'w3m https://duckduckgo.com' C-m

# neomutt
 new-window    -t $srv:4 -n 'mails'
 send-keys     -t  'mails'  \ 'neomutt'

# calcurse
 new-window    -t $srv:5 -n 'calcurse'
 send-keys     -t  'calcurse'  \ 'rm $HOME/.local/share/calcurse/.calcurse.pid ; calcurse -P ; calcurse  -i  $HOME/calcurse.ical ; calcurse --read-only ' C-m

 select-window -t 5
#----------------------------------------------------------------------
srv="development"

# start new session #
 new-session -d -s $srv
 attach-session -t $srv

# rename the first window and start a terminal #
 rename-window -t $srv:0 'code'
 send-keys     -t $srv:0 \ 'cd /home/$USER/Documents/development/gitprojects/ && clear && nvim .' C-m

 new-window    -t $srv:9 -n 'tst_sht_cd'
 send-keys     -t 'tst_sht_cd' \ 'cd $HOME/Documents/development/test_shit_code/ && nvim .' C-m

 select-window -t 0

#----------------------------------------------------------------------
srv="aux"

# start new session #
 new-session -d -s $srv
 attach-session -t $srv

# rename the first window and start a terminal #
 rename-window -t $srv:0 'code'
 send-keys     -t $srv:0  \ 'cd /home/$USER/Documents/development/gitprojects/ && clear' C-m

 new-window    -t $srv:9 -n 'surfing'
 send-keys     -t 'surfing' \ 'cd $HOME && clear' C-m

 new-window    -t $srv:8 -n 'tst_sht_cd'
 send-keys     -t 'tst_sht_cd' \ 'cd $HOME/Documents/development/test_shit_code/ && clear' C-m

 select-window -t 0

#----------------------------------------------------------------------
#attach-session -t $s1


#send-keys     -t  0  "tmux attach-session -t $s1"  C-m
#send-keys     -t  0  "tmux kill-session -t $(tmux list-sessions | awk '{print $1}' | head -1 )"  C-m
#send-keys  -t 0  "tmux kill-session -t $(tmux list-sessions | awk '{print $1}' | head -1 )"

#send-keys -t $s1 "tmux list-sessions | awk 'BEGIN{FS=\"[^0-9]\"}{print $1}' | xargs -n 1 tmux kill-session -t"
