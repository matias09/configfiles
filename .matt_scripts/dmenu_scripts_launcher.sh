#!/usr/bin/env bash

DIR="/home/$USER/Documents/library/books"

if [[ ! -d ${DIR} ]]; then
  echo "--MSG: '${DIR}' folder NOT FOUND"
  exit 0
fi

SUBJECT=$(ls ${DIR}/ | dmenu -l 50 -i)

if [[ ${SUBJECT} == "" ]]; then
  echo "--MSG: Book Category NOT Selected"
  exit 0
fi

SELECTION=$(ls ${DIR}/${SUBJECT}/ | dmenu -l 50 -i)
SELECTION_PATH="${DIR}/${SUBJECT}/${SELECTION}"

FILE_EXTENSION=$(ls ${SELECTION_PATH} | cut -d . -f 2)

prg=""
if [[ $FILE_EXTENSION == "chm" ]]; then
  prg="xchm"
elif [[ $FILE_EXTENSION == "pdf" ]]; then
  prg="zathura"
else
  exit 1
fi

${prg} ${SELECTION_PATH} &
