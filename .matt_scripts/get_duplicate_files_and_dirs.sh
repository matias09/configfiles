#!/usr/bin/env bash

src_dir_path=$1
dst_dir_path=$2
repeated_files_folder_destination=$3

src_files_or_dirs_list=($(ls -1 $src_dir_path))
dst_folder_inside_src_folder_list=($(ls -1  $dst_dir_path))

all_files_repeated=""
expected_files_repeated=0

for f in ${src_files_or_dirs_list[@]}
do
  shorter_file_name=${f}

  if [[ ${#shorter_file_name} > 20 ]]
  then
    shorter_file_name=${f:0:15}
  fi

  files_repeated=($(find $dst_dir_path -name "${shorter_file_name}*" -type f ))

  if [[ ${#files_repeated[@]} > $expected_files_repeated ]]
  then
    f1_shasum=$(shasum ${src_dir_path}/${f} | cut -d ' ' -f 1)

    echo "----------------------------------------------------------------"
    echo " -- File            : "$f
    echo " -- Needle          : "$shorter_file_name
    echo " -- Src             : "${src_dir_path}
    echo " -- shasum          : "${f1_shasum}

    echo ""
    echo "-- Similar File Names Founded :"

    for fr in ${!files_repeated[@]}
    do
      f2_shasum=$(shasum ${files_repeated[ $fr + $expected_files_repeated ]} | cut -d ' ' -f 1)

      if [[ ${f1_shasum} = ${f2_shasum} ]]
      then
        all_files_repeated="${all_files_repeated} ${files_repeated[ $fr + $expected_files_repeated ]}"

        echo " -- Src             : "${files_repeated[ $fr + $expected_files_repeated ]}
        echo " -- shasum          : "${f2_shasum}
      fi
    done

    echo " ----------------------------------------------------------------"
  fi

  files_repeated=""
done

if [[ $all_files_repeated != '' ]]
then
  echo ""
  echo "．━━━━━━━━━━━━━━━━━━━━━━━━━━━━ † ━━━━━━━━━━━━━━━━━━━━━━━━━━━━ ．"
  echo ""

  echo "-- All Files Repeated:"

  echo "Show all Repeated Files ? [ 'y' | 'n' ]"
  echo -n "-->>> "
  read -r see_result

  if [[ $see_result == 'Y' || $see_result == 'y' ]]
  then
    echo ""
    echo "See all Results:"
    for f in $all_files_repeated
    do
      echo $f
    done
  fi

  # TODO: Not Tested Yet
  if [[ ${repeated_files_folder_destination} != '' ]]
  then
    echo "" ; echo ""
    echo "**CAREFULL, FEATURE NOT TESTED YET**"
    echo "Move the following Repeated Files to designated folder  ? [ 'y' | 'n' ]"
    echo " ==>> ( ${repeated_files_folder_destination} )"
    echo ""
    echo "Files:"
    for f in $all_files_repeated
    do
      echo "mv ${f} ${repeated_files_folder_destination}"
    done

    echo ""
    echo "----> Execute  ? [ 'y' | 'n' ]"
    echo ""
    echo -n "-->>> "
    read -r see_result
    if [[ $see_result == 'Y' || $see_result == 'y' ]]
    then
      echo ""
      echo "Moving files:"
      for f in $all_files_repeated
      do
        # echo "mv ${f} ${repeated_files_folder_destination}"
        mv ${f} ${repeated_files_folder_destination}
      done
    fi
  fi
fi
