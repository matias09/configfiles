#!/usr/bin/env bash
RED=`tput setaf 1`
GRE=`tput setaf 2`
MAG=`tput setaf 3`
NC=`tput sgr0` # No Color

# to know if we are in Wayland
XDG_SESSION_TYPE="wayland"

TMP_DIR="/tmp"
MNT_DIR="/mnt"
MAIN_DOCUMENTS_PATH="/home/$USER/Documents"
CONFIGFILES_PATH="${MAIN_DOCUMENTS_PATH}/configfiles"
HOME_CONFIG_PATH="/home/$USER/.config"
ROOT_PRVL_CMD="doas"

RSYNC_CMD_ARGS=" -Curzv --no-links --info=progress2 "
RSYNC_CMD="rsync ${RSYNC_CMD_ARGS}"

EXTERNAL_HARD_DRIVES_FOLDERS="3m bck_concerts bck_library_1 bck_library_2
                              bck_music bck_toshiba library mby3 miii mmm
                              movies music_concerts_cinematics_shows mym
                              new_hd_1_1 new_hd_1_2 new_hd_2_1 new_hd_2_2
                              pen_black pen_blue pen_gris series tm toshiba
                              vancouver"

CANCEL_OPT_STR="cancel"

XORG_SOURCE_PACKAGES=(xorg xorg-xinit libexif xclip) # perhaps "xorg-server" is not needed
XORG_UTILITIES=(autorandr dmenu dwm st sxiv)

WAYLAND_SOURCE_PACKAGES=(wayland wayland-protocols libinput wlroots wl-clipboard)
WAYLAND_UTILITIES=(dwl foot kanshi swaybg swayimg tofi wlr-randr)

PACMAN_INSTALL_CMD="${ROOT_PRVL_CMD} pacman -S --needed --noconfirm "

harddrive_data_source=""
display_server_chosen=""

# ---------------------------------------------------------------------------
# Utilities functions
# ---------------------------------------------------------------------------

USE_CALLBACK=1
DONT_USE_CALLBACK=0

FAILURE_ERROR_CODE_TO_RETURN=-1
FAILURE_ERROR_CODE_RETURNED=255
CANCEL_CODE=0

printSeparator() {
  echo $'\n' \
  "${GRE}----------------------------------------------------------${NC}" \
  "${GRE}----------------------------------------------------------${NC}" $'\n'
}

printMsg() {
  printSeparator
  echo " ${MAG}[------- ${NC}${1}${MAG} -------]${NC}"
}

printAboutToExcecuteMsg() {
  echo $'\t'"${MAG}>>>${NC} ${1}"
}

printWarningMsg() {
  printSeparator
  echo " ${RED}[------- ${NC}${1}${RED} -------]${NC}"
}

cancel() {
  echo " ${MAG}[------- ${NC}Operation Cancelled${MAG} -------]${NC}"
  return ${FAILURE_ERROR_CODE_TO_RETURN}
}

prepareMenu() {
  local options=("$@" "cancel" "#")

  currentOpt=""
  numberOpt=$((((${#options[@]} / 2))-1)) # start point from the max length "0"
  echo '('
  for strOpt in ${options[@]}; do
    if [[ ${strOpt} == "#" ]]; then
      echo "['${numberOpt}']='${currentOpt}'"
      let 'numberOpt=numberOpt - 1'
      currentOpt=""
      continue
    fi
    currentOpt="${currentOpt} ${strOpt}"
  done
  echo ')'
}
getUserSelection() {
  willUseCallbacks=${1}
  shift
  local menuOptions=("$@")
  declare -A menu=$(prepareMenu "${menuOptions[@]}")

  if [[ ${willUseCallbacks} -eq 1 ]]; then
    declare -A menuCallbacks
    for key in ${!menu[@]}; do
      menuCallbacks[${key}]=$(echo ${menu[${key}]} | sed 's/ //g')
    done
  fi

  echo "-- Options:"
  for key in ${!menu[@]}; do
    echo -n $'\t' " - [${key}] ${menu[${key}]}"
  done
  echo $'\n' "------------------------------"
  echo "-- Choose an option:"

  local user_selection=""
  valToReturn=""
  echo -n "--> " && read user_selection
  while :
  do
    if   [[ ${user_selection} -lt 0 ]] \
      || [[ -z ${menu[${user_selection}]} ]];
    then
      echo "Option Not Found . . ." $'\n'
      echo -n "--> " && read user_selection
      continue
    fi
    break
  done

  if [[ ${willUseCallbacks} -eq 1 ]]; then
    eval "${menuCallbacks[${user_selection}]}"
    valToReturn=$?
  else
    valToReturn=${user_selection}
  fi

  if [[ -z ${valToReturn} ]] || [[ ${valToReturn} == 0 ]]; then
    return 0
  fi
  return ${valToReturn}
}

# ---------------------------------------------------------------------------
# Installation functions
# ---------------------------------------------------------------------------
CreatingNeededDirectories() {
  printMsg "Creating MNT directories"
  cd ${TMP_DIR}
  mkdir ${EXTERNAL_HARD_DRIVES_FOLDERS}
  echo "-- ${ROOT_PRVL_CMD} mv ${EXTERNAL_HARD_DRIVES_FOLDERS} ${MNT_DIR}"
  ${ROOT_PRVL_CMD} mv ${EXTERNAL_HARD_DRIVES_FOLDERS} ${MNT_DIR}
}

LookingForBackupHarddrive() {
  printMsg "Looking for backup Harddrive"
  for f in ${EXTERNAL_HARD_DRIVES_FOLDERS} ; do
    tmp_dir="${MNT_DIR}/${f}/environment"
    if [[ -d ${tmp_dir} ]]; then
      harddrive_data_source="${MNT_DIR}/${f}"
      break
    fi
  done
}

Installing_DisplayServer() {
  printMsg "Do you wish to install ?"
  local options=(
    "xorg" "#" # 2
    "wayland" "#")   # 1

  getUserSelection ${DONT_USE_CALLBACK} "${options[@]}"
  user_selection=$?
  if [[ ${user_selection} -eq ${CANCEL_CODE} ]]; then
    exit 0
  elif [[ ${user_selection} -eq 2 ]]; then
    printMsg "Installing Xorg"
    display_server_chosen="xorg"
    echo "-- ${PACMAN_INSTALL_CMD} ${XORG_SOURCE_PACKAGES[@]}"
    ${PACMAN_INSTALL_CMD} ${XORG_SOURCE_PACKAGES[@]}
  else
    printMsg "Installing Wayland"
    display_server_chosen="wayland"
    echo "-- ${PACMAN_INSTALL_CMD} ${WAYLAND_SOURCE_PACKAGES[@]}"
    ${PACMAN_INSTALL_CMD} ${WAYLAND_SOURCE_PACKAGES[@]}
  fi

  printMsg "Which graphics driver you need ?"
  local options_2=(
    "AMD" "#"         # 2
    "INTEL" "#")      # 1

  getUserSelection ${DONT_USE_CALLBACK} "${options_2[@]}"
  user_selection=$?
  if [[ ${user_selection} -eq ${CANCEL_CODE} ]]; then
    exit 0
  elif [[ ${user_selection} -eq 1 ]]; then
    printMsg "Installing INTEL Graphic Drivers"
    echo "-- ${PACMAN_INSTALL_CMD} mesa vulkan-intel xf86-video-intel"
    ${PACMAN_INSTALL_CMD} mesa vulkan-intel xf86-video-intel
  else
    printMsg "Installing AMD Graphic Drivers"
    echo "-- ${PACMAN_INSTALL_CMD} mesa vulkan-radeon xf86-video-ati xf86-video-amdgpu"
    ${PACMAN_INSTALL_CMD} mesa vulkan-radeon xf86-video-ati xf86-video-amdgpu
  fi
}

Installing_SoundServer() {
  printMsg "Installing Pipewire"
  echo "-- ${PACMAN_INSTALL_CMD} libpipewire pipewire pipewire-alsa ... and many more" \
       $'\t' " pipewire-audio libwireplumber wireplumber                      " \
       $'\t' " alsa-card-profiles alsa-lib alsa-plugins alsa-topology-conf    " \
       $'\t' " alsa-ucm-conf alsa-utils"

  ${PACMAN_INSTALL_CMD} libpipewire pipewire pipewire-alsa          \
     pipewire-audio libwireplumber wireplumber                      \
     alsa-card-profiles alsa-lib alsa-plugins alsa-topology-conf    \
     alsa-ucm-conf alsa-utils

   cp -rfv /usr/share/pipewire /home/matt/.config
   systemctl --user start pipewire.service
}

Installing_RemainingPrograms() {
  printMsg "Installing Remaining Programs"
  echo "-- ${PACMAN_INSTALL_CMD} w3m tmux calcurse cmus gdb gcc valgrind podman ccls " \
   $'\t' " zathura zathura-pdf-mupdf mpv pigz base-devel tree " \
   $'\t' " bluez bluez-libs bluez-tools bluez-utils unzip rsync htop ntfs-3g"

  ${PACMAN_INSTALL_CMD} w3m tmux calcurse cmus gdb gcc valgrind podman ccls \
   zathura zathura-pdf-mupdf mpv pigz base-devel tree  \
   bluez bluez-libs bluez-tools bluez-utils unzip rsync htop ntfs-3g
}

Installing_VirtManager() {
  printMsg "Installing Virt-Manager"
  echo "-- ${PACMAN_INSTALL_CMD} virt-manager dmidecode qemu vde2 dnsmasq "\
       $'\t' " bridge-utils openbsd-netcat "

  ${PACMAN_INSTALL_CMD} virt-manager dmidecode qemu vde2 dnsmasq \
     bridge-utils openbsd-netcat

  # Just in case
  # qemu-base qemu-common qemu-img qemu-system-x86 qemu-system-x86-firmware

  echo "-- ${ROOT_PRVL_CMD} systemctl enable libvirtd.service"
  ${ROOT_PRVL_CMD} systemctl enable libvirtd.service
  echo "-- ${ROOT_PRVL_CMD} systemctl start  libvirtd.service"
  ${ROOT_PRVL_CMD} systemctl start  libvirtd.service

  echo "-- ${ROOT_PRVL_CMD} usermod -a -G libvirt ${USER}"
  ${ROOT_PRVL_CMD} usermod -a -G libvirt ${USER}
  echo "-- ${ROOT_PRVL_CMD} newgrp libvirt"
  ${ROOT_PRVL_CMD} newgrp libvirt

  printWarningMsg "Reboot NOW!!!"
}

Installing_Ytdl() {
 printMsg "Installing yt-dl ( youtube-dl replacement )"
  echo "-- ${ROOT_PRVL_CMD} curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o /usr/local/bin/yt-dlp"
  ${ROOT_PRVL_CMD} curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o /usr/local/bin/yt-dlp
  echo "-- ${ROOT_PRVL_CMD} chmod a+rx /usr/local/bin/yt-dlp  # Make executable"
  ${ROOT_PRVL_CMD} chmod a+rx /usr/local/bin/yt-dlp  # Make executable
}

CreatingCustomDirectories() {
  printMsg "Creating custom directories"
  mkdir ${MAIN_DOCUMENTS_PATH}/{library,library/books,library/books/game_development_books,library/courses}
}

DownloadingConfigfilesRepo() {
  printMsg "Downloading Configfiles Repo"
  cd ${MAIN_DOCUMENTS_PATH}/
  git clone https://bitbucket.org/matias09/configfiles.git
}

CopyingFromHarddrive_Personalstuff_And_Development_Folders() {
  printMsg "Copying from Harddrive personal_stuff and development"

  printAboutToExcecuteMsg "${RSYNC_CMD}  ${harddrive_data_source}/personal_stuff  ${MAIN_DOCUMENTS_PATH}/"
  ${RSYNC_CMD}  ${harddrive_data_source}/personal_stuff  ${MAIN_DOCUMENTS_PATH}/

  printAboutToExcecuteMsg "${RSYNC_CMD}  ${harddrive_data_source}/development  ${MAIN_DOCUMENTS_PATH}/"
  ${RSYNC_CMD}  ${harddrive_data_source}/development  ${MAIN_DOCUMENTS_PATH}/
}

CopyingConfigFolderstoDotConfig() {
  printMsg "Copying Config Folders to .config"
  cd ${CONFIGFILES_PATH}
  cp -rf ${CONFIGFILES_PATH}/dot_config/{zathura}/ ${HOME_CONFIG_PATH}
  cp -rf ${CONFIGFILES_PATH}/dot_fonts/ /home/${USER}/.fonts
}

MakingZathuraHardSymbolicLinksToConfigDir() {
  printMsg "Making zathura Hard Symbolic Links to Config Dir"
  mkdir /home/${USER}/.config/zathura/
  ln ${CONFIGFILES_PATH}/dot_config/zathura/* /home/${USER}/.config/zathura/
}

MakingPicomHardSymbolicLinkstoConfigDir() {
  printMsg "Making picom Hard Symbolic Links to Config Dir"
  if [[ ! -d /home/${USER}/.config/picom/ ]]; then
    mkdir /home/${USER}/.config/picom/
  else
    rm /home/${USER}/.config/picom/* -rf
  fi
  ln ${CONFIGFILES_PATH}/dot_config/picom/* /home/${USER}/.config/picom/
}

MakingNeomuttHardSymbolicLinksToConfigDir() {
  printMsg "Making neomutt Hard Symbolic Links to Config Dir"
  mkdir /home/${USER}/.config/neomutt
  ln ${CONFIGFILES_PATH}/dot_config/neomutt/* /home/${USER}/.config/neomutt/
}

MakingCalcurseHardSymbolicLinksToConfigDir() {
  printMsg "Making calcurse Hard Symbolic Links to Config Dir"
  mkdir /home/${USER}/.config/calcurse
  ln ${CONFIGFILES_PATH}/dot_config/calcurse/* /home/${USER}/.config/calcurse/
}

MakingHardSymbolicLinksToUserDir() {
  printMsg "Making Hard Symbolic Links to User Dir"
  ln ${MAIN_DOCUMENTS_PATH}/personal_stuff/calcurse.ical /home/${USER}/

  dot_home_config_files=".gitconfig .gitignore .gitignore_global .NERDTreeBookmarks .tmux.conf .dir_colors"
  for f in ${dot_home_config_files} ; do
    ln ${CONFIGFILES_PATH}/home_dir_dot_files/${f} /home/${USER}/.w3m/
  done

  mkdir -p /home/${USER}/.w3m
  for f in $(ls -1 ${CONFIGFILES_PATH}/dot_w3m/*) ; do
    ln ${f} /home/${USER}/.w3m/
  done
}

Installing_Cmake() {
  printMsg "cmake installation"
  cd ${MAIN_DOCUMENTS_PATH}/development/tools/
  tar -xzf cmake*.tar.gz
  rm cmake*.tar.gz

  # --- Commented out because it's a time consuming process --- #
  # printMsg "Configuring"
  # ./configure
  # echo "Building"
  # make
  # echo "Installing_"
  # ${ROOT_PRVL_CMD} make install
}

Installing_XWallpaper() {
  printMsg "XWallpaper installation"
  cd ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/xwallpaper/
  make clean
  ./configure
  make
  echo "-- ${ROOT_PRVL_CMD} ln -sf $(pwd)/xwallpaper /usr/bin/xwallpaper"
  ${ROOT_PRVL_CMD} ln -sf $(pwd)/xwallpaper /usr/bin/xwallpaper
}

Installing_XSVI() {
  printMsg "xsvi installation"
  cd ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/sxiv/
  make clean
  ./configure
  make
  echo "-- ${ROOT_PRVL_CMD} ln -sf $(pwd)/sxiv /usr/bin/sxiv"
  ${ROOT_PRVL_CMD} ln -sf $(pwd)/sxiv /usr/bin/sxiv
}

Installing_Nvim() {
  printMsg "nvim installation"

  # App Image dependency
  echo "${PACMAN_INSTALL_CMD} fuse"
  ${PACMAN_INSTALL_CMD} fuse

  mkdir ${HOME_CONFIG_PATH}/nvim
  cd ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/ides/text_based/nvim/
  cp -rf plugin_manager/nvim/ /home/$USER/.local/share/

  cd ${MAIN_DOCUMENTS_PATH}/development/tools/
  tar -xf node-*.tar.gz
  rm node-*.tar.gz

  echo "-- f=node; ${ROOT_PRVL_CMD} ln -sf $PWD/node-*/bin/${f} /usr/bin/${f}"
  f=node; ${ROOT_PRVL_CMD} ln -sf $PWD/node-*/bin/${f} /usr/bin/${f}
  echo "-- f=npm; ${ROOT_PRVL_CMD} ln -sf $PWD/node-*/bin/${f} /usr/bin/${f}"
  f=npm; ${ROOT_PRVL_CMD} ln -sf $PWD/node-*/bin/${f} /usr/bin/${f}

  cd ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/appimages/
  mv nvim*.appimage nvim.appimage
  chmod +x nvim.appimage
  echo "-- ${ROOT_PRVL_CMD} ln -s  $(pwd)/nvim.appimage /usr/bin/nvim"
  ${ROOT_PRVL_CMD} ln -s  $(pwd)/nvim.appimage /usr/bin/nvim

  ln ${CONFIGFILES_PATH}/ides/text_based/nvim/init.vim ${HOME_CONFIG_PATH}/nvim/

  mkdir -p /home/$USER/.vim/
  cp ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/ides/text_based/vim/plugins/nerd_tree.zip /home/$USER/.vim/
  cd /home/$USER/.vim/
  unzip nerd_tree.zip
  rm nerd_tree.zip
  cd

  ln ${CONFIGFILES_PATH}/ides/text_based/nvim/coc-settings.json  ${HOME_CONFIG_PATH}/nvim/
  ln -sf ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/ides/text_based/colors/ /home/$USER/.vim/colors
}

PreparingGitProjectDirectoryAndTheRepositories() {
  printMsg "Preparing GitProject directory and the repositories"
  mkdir -p ${MAIN_DOCUMENTS_PATH}/development/gitprojects/zz_environment
  cp -rfv ${harddrive_data_source}/environment/${display_server_chosen}_programs/* ${MAIN_DOCUMENTS_PATH}/development/gitprojects/zz_environment
}

Installing_XorgUtilities() {
  for u in ${XORG_UTILITIES}; do
    printMsg "${u@U} installation"
    echo "${MAIN_DOCUMENTS_PATH}/development/gitprojects/zz_environment/${u}"
    cd ${MAIN_DOCUMENTS_PATH}/development/gitprojects/zz_environment/${u}

    echo "-- make clean && make && ${ROOT_PRVL_CMD} make install"
    make clean && make && ${ROOT_PRVL_CMD} make install
  done
}

Installing_WaylandUtilities() {
  for u in ${WAYLAND_UTILITIES[@]}; do
    printMsg "${u@U} installation"
    echo "${MAIN_DOCUMENTS_PATH}/development/gitprojects/zz_environment/${u}"
    cd ${MAIN_DOCUMENTS_PATH}/development/gitprojects/zz_environment/${u}

    if [[ ${u} == "dwl" ]]; then
      echo "-- make clean && make && ${ROOT_PRVL_CMD} make install"
      make clean && make && ${ROOT_PRVL_CMD} make install
    else
      echo "-- ${ROOT_PRVL_CMD} ln -s ${PWD}/build/${u} /usr/bin/"
      ${ROOT_PRVL_CMD} ln -s ${PWD}/build/${u} /usr/bin/
    fi
  done
}

Installing_Ly() {
  printMsg "ly installation"
  cp -rf ${harddrive_data_source}/environment/session_loaders/ly.tar ${MAIN_DOCUMENTS_PATH}/development/gitprojects/zz_environment/
  cd ${MAIN_DOCUMENTS_PATH}/development/gitprojects/zz_environment/ && \
      tar -xf ly.tar                                                && \
      rm ly.tar                                                     && \
      cd ly/ && make clean && make                                  && \
      echo "-- ${ROOT_PRVL_CMD} make install && ${ROOT_PRVL_CMD} make installsystemd" && \
      ${ROOT_PRVL_CMD} make install && ${ROOT_PRVL_CMD} make installsystemd

  echo "-- ${ROOT_PRVL_CMD} systemctl enable ly"
  ${ROOT_PRVL_CMD} systemctl enable ly
  if [[ ${display_server_chosen} == "xorg" ]]; then
    echo "${ROOT_PRVL_CMD} mkdir /usr/share/xsessions/"
    ${ROOT_PRVL_CMD} mkdir /usr/share/xsessions/
    echo "-- ${ROOT_PRVL_CMD} cp ${CONFIGFILES_PATH}/.matt_scripts/desktop_environment/loaders/dwm.desktop /usr/share/xsessions/"
    ${ROOT_PRVL_CMD} cp ${CONFIGFILES_PATH}/.matt_scripts/desktop_environment/loaders/dwm.desktop /usr/share/xsessions/
  else
    echo "${ROOT_PRVL_CMD} mkdir /usr/share/wayland-sessions"
    ${ROOT_PRVL_CMD} mkdir /usr/share/wayland-sessions
    echo "-- ${ROOT_PRVL_CMD} cp ${CONFIGFILES_PATH}/.matt_scripts/desktop_environment/loaders/dwl.desktop /usr/share/wayland-sessions/"
    ${ROOT_PRVL_CMD} cp ${CONFIGFILES_PATH}/.matt_scripts/desktop_environment/loaders/dwl.desktop /usr/share/wayland-sessions/
  fi
}

CopyingWallpapers() {
  printMsg "Copying wallpapers"
  printAboutToExcecuteMsg "${ROOT_PRVL_CMD} ${RSYNC_CMD}  ${harddrive_data_source}/personal_stuff  ${MAIN_DOCUMENTS_PATH}/"
  ${ROOT_PRVL_CMD} ${RSYNC_CMD} ${harddrive_data_source}/wallpapers /usr/share/
}

RemovingAnnoyingUserDirectoryFolders() {
  printMsg "Removing Annoying User Directory Folders"
  for f in "Templates Videos Desktop Music Public Pictures"; do
    if [[ -d ${f} ]]; then
      rm /home/$USER/${f}/ -rf
    fi
  done
}


CheckDependencies() {
  echo "${MAG}    -- The next flow of configurations would require your attention." $'\n' \
       "          -- Are you up to this or you'll walk away quickly ?${NC}"

  local options=(
    "yes" "#" # 1
  )

  getUserSelection ${DONT_USE_CALLBACK} "${options[@]}"
  declare user_selection=$?
  if [[ ${user_selection} -eq ${CANCEL_CODE} ]]; then
    exit 0
  fi

  LookingForBackupHarddrive

  if [[ ! -d ${harddrive_data_source} ]]; then
    echo $'\t' "-- Environment confg files, not found." $'\n' \
         $'\t' "-- External disk not mounted."
    exit 1
  fi

  deps="doas exfat-utils"
  for p in ${deps}; do
    e=$(whereis ${p})
    r=${e/${p}:/}
    [[ ! -v "r" ]] && echo "${p} is Not Installed" \
                   && echo "Bye ..." \
                   && exit 1
  done
}

# --------------- #
Main() {
  CheckDependencies
  CreatingNeededDirectories
  Installing_DisplayServer
  Installing_SoundServer
  Installing_RemainingPrograms
  Installing_VirtManager
  Installing_Ytdl
  CreatingCustomDirectories

  # At the moment doing it with the installation script
  # DownloadingConfigfilesRepo

  CopyingFromHarddrive_Personalstuff_And_Development_Folders
  MakingZathuraHardSymbolicLinksToConfigDir
  MakingNeomuttHardSymbolicLinksToConfigDir
  MakingCalcurseHardSymbolicLinksToConfigDir
  MakingHardSymbolicLinksToUserDir
  Installing_Cmake
  Installing_Nvim

  # -- No longer required, We are copying the entire "development" folder -- #
  # PreparingGitProjectDirectoryAndTheRepositories

  if [[ ${display_server_chosen} == "xorg" ]]; then
    MakingPicomHardSymbolicLinkstoConfigDir
    Installing_XorgUtilities
  else
    Installing_WaylandUtilities
  fi
  Installing_Ly
  CopyingWallpapers
  RemovingAnnoyingUserDirectoryFolders
}

# Run #
Main
