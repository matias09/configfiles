#/usr/bin/env bash
export LOG_FILE="/tmp/DWL_INIT_LOG"
export ENV_VIEW_SELECTED="1v_10v 1v 10v"
export WALLPAPERS_FOLDER="/usr/share/wallpapers"

export ROOT_CUSTOM_SCRIPTS="/usr/share/custom_scripts"
export KANSHI_PROFILES_PATH="${ROOT_CUSTOM_SCRIPTS}/kanshi"
export KANSHI_PROFILES=$(ls -1 ${KANSHI_PROFILES_PATH})
