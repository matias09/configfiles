#/bin/env bash
LOG_FILE="/tmp/DWL_INIT_LOG"

echo "-------------------- set wallpapers --------------------" >> ${LOG_FILE}

[[ ! -v "XDG_SESSION_TYPE" ]] && echo "ENV variable XDG_SESSION_TYPE not set" >> ${LOG_FILE} \
                              && echo "Bye ..." >> ${LOG_FILE} \
                              && return 1

# export WALLPAPERS_FOLDER="/usr/share/wallpapers"

[[ ! -v "WALLPAPERS_FOLDER" ]] && echo "ENV variable WALLPAPERS_FOLDER not set" >> ${LOG_FILE} \
                               && echo "Bye ..." >> ${LOG_FILE} \
                               && return 1

NORMAL_ORIENTATION="normal"
WAYLAND_SERVER_NAME="wayland"
TMP_CURR_MON_WALLPAPERS="/tmp/curr_mon_wallpapers"

to_which_monitor=${1}

randr_prg=""
wall_prg_set_output=""
wall_prg_set_image_scale=""

if [[ ${XDG_SESSION_TYPE} == ${WAYLAND_SERVER_NAME} ]]; then
  randr_prg="wlr-randr"
  wall_prg="swaybg"
  wall_prg_set_output=" -o "
  wall_prg_set_image_scale=" -m fit -i "
else
  randr_prg="xrandr"
  wall_prg="xwallpaper"
  wall_prg_set_output=" --output "
  wall_prg_set_image_scale=" --maximize "
fi

deps="${wall_prg} ${randr_prg}"
for p in ${deps}; do
  e=$(whereis ${p})
  r=${e/${p}:/}
  [[ ! -v "r" ]] && echo "${p} is Not Installed" \
                 && echo "Bye ..." \
                 && return 1
done

# Current Monitor Randr Conf
mon_names=($(${randr_prg} | grep \" \
                          | grep -E "\w+." \
                          | cut -d \" -f 1 \
                          | cut -d : -f 3))
mon_transform_conf=($(${randr_prg}  | grep Transform | cut -d " " -f 4))

declare -A mon_map_conf

declare -A TRA_MAP_CONF
TRA_MAP_CONF["normal"]="horizontals"
TRA_MAP_CONF["flipped"]="horizontals"
TRA_MAP_CONF["180"]="horizontals"
TRA_MAP_CONF["flipped-180"]="horizontals"

TRA_MAP_CONF["90"]="verticals"
TRA_MAP_CONF["flipped-90"]="verticals"
TRA_MAP_CONF["270"]="verticals"
TRA_MAP_CONF["flipped-270"]="verticals"

ind=0
for mon in ${mon_names[@]}; do
  mon_map_conf[${mon}]=${mon_transform_conf[${ind}]}
  let "ind=ind+1"
done

wall_prg_call_parameters=""
current_wallpapers_on_monitors=()
mon_indx=0

if [[ -z ${to_which_monitor} ]]; then
  for mon in ${mon_names[@]}; do
    orientation_folder=${TRA_MAP_CONF[${mon_map_conf[${mon}]}]}
    wallpapers_path="${WALLPAPERS_FOLDER}/${orientation_folder}"
    wallpaper_to_apply=$(ls ${wallpapers_path}/* | shuf -n 1)

    wall_prg_call_parameters=${wall_prg_call_parameters}"   \
                                ${wall_prg_set_output}      \
                                ${mon}                      \
                                ${wall_prg_set_image_scale} \
                                ${wallpaper_to_apply} "

    current_wallpapers_on_monitors+=(${mon}\|${wallpaper_to_apply})
    let "mon_indx=mon_indx+1"
  done
else
  if [[ ${mon_map_conf[${to_which_monitor}]} == "" ]]; then
      echo "-- The current monitor selected has no configuration --"
      echo  ${mon_map_conf[${to_which_monitor}]}
      return 1
  fi

  for mon in ${mon_names[@]}; do
    wallpaper_to_apply=$(cat ${TMP_CURR_MON_WALLPAPERS} \
                          | grep ${mon} | cut -d "|" -f 2)

    if [[ ${mon} == ${to_which_monitor} ]]; then
      orientation_folder=${TRA_MAP_CONF[${mon_map_conf[${mon}]}]}
      wallpapers_path="${WALLPAPERS_FOLDER}/${orientation_folder}"
      wallpaper_to_apply=$(ls ${wallpapers_path}/* | shuf -n 1)
    fi

    wall_prg_call_parameters=${wall_prg_call_parameters}"   \
                                ${wall_prg_set_output}      \
                                ${mon}                      \
                                ${wall_prg_set_image_scale} \
                                ${wallpaper_to_apply} "

    current_wallpapers_on_monitors+=(${mon}\|${wallpaper_to_apply})
    let "mon_indx=mon_indx+1"
  done
fi

if [[ -a ${TMP_CURR_MON_WALLPAPERS} ]]; then
  > ${TMP_CURR_MON_WALLPAPERS} # '>'  With this way, you clean all the file
fi

for c in ${current_wallpapers_on_monitors[@]}; do
  echo ${c} >> ${TMP_CURR_MON_WALLPAPERS}
done

killall ${wall_prg}
${wall_prg} ${wall_prg_call_parameters} &
