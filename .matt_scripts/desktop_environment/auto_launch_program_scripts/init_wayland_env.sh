#!/usr/bin/env bash

LOG_FILE="/tmp/DWL_INIT_LOG"
> LOG_FILE

echo "-------------------- dwl init $(date +%H_%M_%S)--------------------" >> ${LOG_FILE}

PrepareOptionList() {
  null=${1}
  options=("$@")

  echo '('
    for strOpt in ${options[@]}; do
      echo "['${strOpt}']='${strOpt}'"
    done
  echo ')'
}


[[ ! -v "XDG_SESSION_TYPE" ]] && echo "ENV variable XDG_SESSION_TYPE not set" >> ${LOG_FILE} \
                              && echo "Bye ..." \
                              && return 1

source /etc/profile.d/global_constants.sh

deps="swaybg kanshi"
for p in ${deps}; do
  e=$(whereis ${p})
  r=${e/${p}:/}
  [[ ! -v "r" ]] && echo "${p} is Not Installed" > ${LOG_FILE} \
                 && echo "Bye ..." >> ${LOG_FILE} \
                 && return 1
done

lo=($(ls -1 /usr/share/custom_scripts/kanshi))
declare -A opt_dict=$(PrepareOptionList "${lo[@]}")

conf_found=0
conf_selected=""
for c in ${ENV_VIEW_SELECTED}; do
   if [[ ${opt_dict[${c}]} != "" ]]; then
    conf_found=1
    conf_selected=${c}
    break
  fi
done
echo "-- Conf Selected = ${conf_selected} --------------------" >> ${LOG_FILE}

[[ ${conf_found} -eq 0 ]] &&  echo "Kanshi conf Not Expected..." >> ${LOG_FILE} \
                          &&  echo "ENV_VIEW_SELECTED: ${conf_selected}" >> ${LOG_FILE} \
                          &&  echo "KANSHI_PROFILES_PATH: ${KANSHI_PROFILES_PATH}" >> ${LOG_FILE} \
                          &&  echo "KANSHI_PROFILES: ${KANSHI_PROFILES}" >> ${LOG_FILE} \
                          &&  return 1

kanshi -c ${KANSHI_PROFILES_PATH}/${conf_selected} &
bash ${ROOT_CUSTOM_SCRIPTS}/set_wallpapers.sh
