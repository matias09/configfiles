#/usr/bin/env bash
WALLPAPERS_FOLDER_LOCAL="${WALLPAPERS_FOLDER}/horizontals/"
WALLPAPERS_FOLDER_EXTERNAL="/mnt/3m/wallpapers/horizontals/"

echo "Modifying names ..."
for f in $(ls mpv*.jpg); do
  n=$(ls ${f}*)
  f2=${n:0:-4}
  mv ${n} ${f2}_$(date +%Y_%m_%d__%H_%M_%S__%N).jpg
done

if [[ ! -d ${WALLPAPERS_FOLDER_LOCAL} ]] ; then
  echo $'\t' "-- Wallpaper local folder, not found." $'\n'
  exit 1
fi

echo "Copying  ..."
cp mpv* ${WALLPAPERS_FOLDER_LOCAL}


if [[ ! -d ${WALLPAPERS_FOLDER_EXTERNAL} ]] ; then
  echo $'\t' "-- Wallpaper external folder, not found." $'\n' \
       $'\t' "-- Possibly the external disk is not mounted."
  exit 1
fi

echo "Moving  ..."
mv mpv-* ${WALLPAPERS_FOLDER_EXTERNAL}
