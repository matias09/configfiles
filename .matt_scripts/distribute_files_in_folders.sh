#/usr/bin/env bash
FILES_PER_FOLDER=20

folder_indx=1
mkdir ${folder_indx}

indx=1
for file in $(ls -1 *.jpg) ; do
  if [[ ${indx} -eq ${FILES_PER_FOLDER} ]]; then
    let "folder_indx=folder_indx+1"
    mkdir ${folder_indx}
    indx=0
  fi
  mv ${file} ${folder_indx}
  let "indx=indx+1"
done

for f in {1..9} ; do mv ${f}/ 0${f}/; done
