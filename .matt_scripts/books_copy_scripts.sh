#!/usr/bin/env bash

MNT_DIR="/mnt"
EXTERNAL_HARD_DRIVES_FOLDERS="3m bck_concerts bck_library_1 bck_library_2 bck_music bck_toshiba library mby3 miii mmm movies music_concerts_cinematics_shows mym new_hd_1_1 new_hd_1_2 new_hd_2_1 new_hd_2_2 pen_black pen_blue pen_gris series tm toshiba vancouver"

echo
echo "---- Looking for backup Harddrive ----"
harddrive_data_source=""
for f in ${EXTERNAL_HARD_DRIVES_FOLDERS} ; do
  tmp_dir="${MNT_DIR}/${f}/environment"
  if [[ -d ${tmp_dir} ]] ; then
    harddrive_data_source="${MNT_DIR}/${f}"
    break
  fi
done

if [[ ! -d ${harddrive_data_source} ]] ; then
  echo $'\t' "-- Environment confg files, not found." $'\n' \
       $'\t' "-- External disk not mounted."
  exit 1
fi

LOCAL_BOOKS_DIR="/home/${USER}/Documents/library/books"
EXT_BOOKS_DIR="/mnt/3m/library/books"
RSYNC_CMD_ARGS=" -Curzv --no-links --info=progress2 "
RSYNC_CMD="rsync ${RSYNC_CMD_ARGS}"

${RSYNC_CMD} ${EXT_BOOKS_DIR}/{algorithms,c,cpp,graphics_libraries,graphics_programming,software_design} ${LOCAL_BOOKS_DIR}/

${RSYNC_CMD} ${EXT_BOOKS_DIR}/{engines,lua,mathematics,physics,xxx_game_programming_in_cpp_creating_3d_games,zzz_various_but_very_important} ${LOCAL_BOOKS_DIR}/game_development_books/
