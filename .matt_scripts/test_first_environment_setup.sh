#!/usr/bin/env bash
RED=`tput setaf 1`
GRE=`tput setaf 2`
MAG=`tput setaf 3`
NC=`tput sgr0` # No Color

# to know if we are in Wayland
XDG_SESSION_TYPE="wayland"

TMP_DIR="/tmp"
MNT_DIR="/mnt"
MAIN_DOCUMENTS_PATH="/home/$USER/Documents"
CONFIGFILES_PATH="${MAIN_DOCUMENTS_PATH}/configfiles"
HOME_CONFIG_PATH="/home/$USER/.config"
ROOT_PRVL_CMD="doas"

EXTERNAL_HARD_DRIVES_FOLDERS="3m bck_concerts bck_library_1 bck_library_2
                              bck_music bck_toshiba library mby3 miii mmm
                              movies music_concerts_cinematics_shows mym
                              new_hd_1_1 new_hd_1_2 new_hd_2_1 new_hd_2_2
                              pen_black pen_blue pen_gris series tm toshiba
                              vancouver"


CANCEL_OPT_STR="cancel"

XORG_SOURCE_PACKAGES=(xorg-server)
XORG_UTILITIES=(autorandr dmenu dwm st sxiv)

WAYLAND_SOURCE_PACKAGES=(wayland wayland-protocols)
WAYLAND_UTILITIES=(dwl foot kanshi swaybg swayimg tofi wlr-randr)

PACMAN_INSTALL_CMD="${ROOT_PRVL_CMD} pacman -S --needed --noconfirm "

harddrive_data_source=""
display_server_chosen=""

# ---------------------------------------------------------------------------
# Utilities functions
# ---------------------------------------------------------------------------

USE_CALLBACK=1
NOT_USE_CALLBACK=0

FAILURE_ERROR_CODE_TO_RETURN=-1
FAILURE_ERROR_CODE_RETURNED=255
CANCEL_CODE=0

printSeparator() {
  echo $'\n' \
  "${GRE}----------------------------------------------------------${NC}" \
  "${GRE}----------------------------------------------------------${NC}" $'\n'
}

printMsg() {
  printSeparator
  echo " ${MAG}[------- ${NC}${1}${MAG} -------]${NC}"
}

printWarningMsg() {
  printSeparator
  echo " ${RED}[------- ${NC}${1}${RED} -------]${NC}"
}

cancel() {
  echo " ${MAG}[------- ${NC}Operation Cancelled${MAG} -------]${NC}"
  return ${FAILURE_ERROR_CODE_TO_RETURN}
}

prepareMenu() {
  local options=("$@" "cancel" "#")

  currentOpt=""
  numberOpt=$((((${#options[@]} / 2))-1)) # start point from the max length "0"
  echo '('
  for strOpt in ${options[@]}; do
    if [[ ${strOpt} == "#" ]]; then
      echo "['${numberOpt}']='${currentOpt}'"
      let 'numberOpt=numberOpt - 1'
      currentOpt=""
      continue
    fi
    currentOpt="${currentOpt} ${strOpt}"
  done
  echo ')'
}
getUserSelection() {
  willUseCallbacks=${1}
  shift
  local menuOptions=("$@")
  declare -A menu=$(prepareMenu "${menuOptions[@]}")

  if [[ ${willUseCallbacks} -eq 1 ]]; then
    declare -A menuCallbacks
    for key in ${!menu[@]}; do
      menuCallbacks[${key}]=$(echo ${menu[${key}]} | sed 's/ //g')
    done
  fi

  echo "-- Options:"
  for key in ${!menu[@]}; do
    echo -n $'\t' " - [${key}] ${menu[${key}]}"
  done
  echo $'\n' "------------------------------"
  echo "-- Choose an option:"

  local user_selection=""
  valToReturn=""
  echo -n "--> " && read user_selection
  while :
  do
    if   [[ ${user_selection} -lt 0 ]] \
      || [[ -z ${menu[${user_selection}]} ]];
    then
      echo "Option Not Found . . ." $'\n'
      echo -n "--> " && read user_selection
      continue
    fi
    break
  done

  if [[ ${willUseCallbacks} -eq 1 ]]; then
    eval "${menuCallbacks[${user_selection}]}"
    valToReturn=$?
  else
    valToReturn=${user_selection}
  fi

  if [[ -z ${valToReturn} ]] || [[ ${valToReturn} == 0 ]]; then
    return 0
  fi
  return ${valToReturn}
}

###########################
# How to use the menu
###########################
# t12() {
#   local options1=(
#     "wayland wayland" "#"
#     "xorg xorg" "#"
#   )
#
#   declare -A menu1=$(prepareMenu "${options1[@]}")
#
#   getUserSelection ${NOT_USE_CALLBACK} "${options1[@]}"
#   declare user_selection=$?
#   echo "-- user_selection from the menu was: ${menu1[${user_selection}]}"
#
#   #---------------------------------------------------------------------
#   echo $'\n'
#   local options2=(
#     "wayland" "#"
#     "xorg" "#"
#   )
#
#   declare -A menu2=$(prepareMenu "${options2[@]}")
#
#   getUserSelection ${USE_CALLBACK} "${options2[@]}"
#   declare user_selection2=$?
#
#   callbackResult="SUCCESS"
#   if [[ ${user_selection2} -eq ${FAILURE_ERROR_CODE_RETURNED} ]]; then
#     callbackResult="FAILURE"
#   fi
#   echo "-- The result of the callback was a : ${callbackResult}"
# }

# ---------------------------------------------------------------------------
# Installation functions
# ---------------------------------------------------------------------------

CreatingMntDirectories() {
  printMsg "Creating MNT directories"
  echo "cd ${TMP_DIR}"
  echo "mkdir ${EXTERNAL_HARD_DRIVES_FOLDERS}"
  echo "${ROOT_PRVL_CMD} mv ${EXTERNAL_HARD_DRIVES_FOLDERS} ${MNT_DIR}"
}

LookingForBackupHarddrive() {
  printMsg "Looking for backup Harddrive"
  for f in ${EXTERNAL_HARD_DRIVES_FOLDERS} ; do
    tmp_dir="${MNT_DIR}/${f}/environment"
    echo "looking for : ${tmp_dir}"
    if [[ -d ${tmp_dir} ]] ; then
      harddrive_data_source="${MNT_DIR}/${f}"
      break
    fi
  done
}

InstallingDisplayServer() {
  printMsg "Do you wish to install ?"
  local options=(
    "wayland" "#" # 2
    "xorg" "#")   # 1

  declare -A menu=$(prepareMenu "${options[@]}")

  getUserSelection 0 "${options[@]}"
  declare user_selection=$?
  if [[ ${user_selection} -eq ${CANCEL_CODE} ]]; then
    exit 0
  fi

  if [[ ${user_selection} -eq 1 ]]; then
    printMsg "Installing Xorg"
    display_server_chosen="xorg"
    echo "${PACMAN_INSTALL_CMD} ${XORG_SOURCE_PACKAGES}"
  else
    printMsg "Installing Wayland"
    display_server_chosen="wayland"
    echo "${PACMAN_INSTALL_CMD} ${WAYLAND_SOURCE_PACKAGES}"
  fi
}

InstallingSoundServer() {
  printMsg "Installing Pipewire"
  echo "${PACMAN_INSTALL_CMD} libpipewire pipewire pipewire-alsa pipewire-audio libwireplumber wireplumber"
  # pipewire-jack # <<-- saved just in case, Hopelly we wouldn't needed
}

InstallingRemainingPrograms() {
  printMsg "Installing Remaining Programs"
  echo "${PACMAN_INSTALL_CMD} w3m  tmux calcurse cmus gdb gcc valgrind podman ccls zathura zathura-pdf-mupdf  mpv mplayer git"
}

InstallingVirtManager() {
  printMsg "Installing Virt-Manager"
  echo "${PACMAN_INSTALL_CMD} virt-manager dmidecode qemu vde2 dnsmasq bridge-utils openbsd-netcat"

  echo "${ROOT_PRVL_CMD} systemctl enable libvirtd.service"
  echo "${ROOT_PRVL_CMD} systemctl start  libvirtd.service"

  echo "echo "${ROOT_PRVL_CMD} usermod -a -G libvirt ${USER}""
  echo "${ROOT_PRVL_CMD} newgrp libvirt"

  printWarningMsg "Reboot NOW!!!"
}

InstallingYtdl() {
 printMsg "Installing yt-dl ( youtube-dl replacement )"
  echo "${ROOT_PRVL_CMD} curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o /usr/local/bin/yt-dlp"
  echo "${ROOT_PRVL_CMD} chmod a+rx /usr/local/bin/yt-dlp  # Make executable"
}

CreatingCustomDirectories() {
  printMsg "Creating custom directories"
  echo "mkdir ${MAIN_DOCUMENTS_PATH}/{personal_stuff,development,development/test_shit_code,library,library/books,library/courses}"
}

DownloadingConfigfilesRepo() {
  printMsg "Downloading Configfiles Repo"
    echo "cd ${MAIN_DOCUMENTS_PATH}/"
    echo "git clone https://bitbucket.org/matias09/configfiles.git"
}

CopyingFromHarddrivePersonalstuff() {
  printMsg "Copying from Harddrive personal_stuff"
  echo "cp     ${harddrive_data_source}/personal_stuff/{untitled,calcurse.ical}  ${MAIN_DOCUMENTS_PATH}/personal_stuff"
  echo "cp -rf ${harddrive_data_source}/personal_stuff/apps/ ${MAIN_DOCUMENTS_PATH}/personal_stuff/"
  echo "cp -rf ${harddrive_data_source}/personal_stuff/companies_docs/ ${MAIN_DOCUMENTS_PATH}/personal_stuff/"
}

CopyingConfigFolderstoDotConfig() {
  printMsg "Copying Config Folders to .config"
  echo "cd ${CONFIGFILES_PATH}"
  echo "cp -rf ${CONFIGFILES_PATH}/dot_config/{zathura/} ${HOME_CONFIG_PATH}"
  echo "cp -rf ${CONFIGFILES_PATH}/dot_fonts/ /home/$USER/.fonts"
}

MakingZathuraHardSymbolicLinksToConfigDir() {
  printMsg "Making zathura Hard Symbolic Links to Config Dir"
  echo "mkdir /home/$USER/.config/zathura/"
  echo "ln ${CONFIGFILES_PATH}/dot_config/zathura/* /home/$USER/.config/zathura/"
}

MakingPicomHardSymbolicLinkstoConfigDir() {
  printMsg "Making picom Hard Symbolic Links to Config Dir"
  if [[ ! -d /home/$USER/.config/picom/ ]]; then
    echo "mkdir /home/$USER/.config/picom/"
  else
    echo "rm /home/$USER/.config/picom/* -rf"
  fi
  echo "ln ${CONFIGFILES_PATH}/dot_config/picom/* /home/$USER/.config/picom/"
}

MakingNeomuttHardSymbolicLinksToConfigDir() {
  printMsg "Making neomutt Hard Symbolic Links to Config Dir"
  echo "mkdir /home/$USER/.config/neomutt"
  echo "ln ${CONFIGFILES_PATH}/dot_config/neomutt/* /home/$USER/.config/neomutt/"
}

MakingCalcurseHardSymbolicLinksToConfigDir() {
  printMsg "Making calcurse Hard Symbolic Links to Config Dir"
  echo "mkdir /home/$USER/.config/calcurse"
  echo "ln ${CONFIGFILES_PATH}/dot_config/calcurse/* /home/$USER/.config/calcurse/"
}

MakingHardSymbolicLinksToUserDir() {
  printMsg "Making Hard Symbolic Links to User Dir"
  echo "ln ${CONFIGFILES_PATH}/{.gitconfig,.gitignore,.gitignore_global,.NERDTreeBookmarks} /home/$USER/"
  echo "ln ${CONFIGFILES_PATH}/.tmux.conf /home/$USER/"
  echo "ln ${MAIN_DOCUMENTS_PATH}/personal_stuff/calcurse.ical /home/$USER/"

  echo "echo "mkdir -p /home/$USER/.w3m""
  for f in $(ls -1 ${CONFIGFILES_PATH}/dot_w3m/*) ; do
    echo "ln ${f} /home/$USER/.w3m/"
  done
}

CopyingFromHarddriveDevelopmentFolders() {
  printMsg "Copying from Harddrive development folders"
  echo "cp -rfv ${harddrive_data_source}/development/{tools,libs,quick_book_reference} ${MAIN_DOCUMENTS_PATH}/development/"
}

InstallingCmake() {
  printMsg "cmake installation"
  echo "cd ${MAIN_DOCUMENTS_PATH}/development/tools/"
  echo "tar -xzf cmake-3.22.2.tar.gz"
  echo "rm cmake-3.22.2.tar.gz"
  echo "f=cmake; ${ROOT_PRVL_CMD} ln -sf  $PWD/cmake-3.22.2/bin/${f} /usr/bin/${f}"
  echo "f=ctest; ${ROOT_PRVL_CMD} ln -sf  $PWD/cmake-3.22.2/bin/${f} /usr/bin/${f}"
  echo "f=cpack; ${ROOT_PRVL_CMD} ln -sf  $PWD/cmake-3.22.2/bin/${f} /usr/bin/${f}"
}

InstallingXWallpaper() {
  printMsg "XWallpaper installation"
  echo "cd ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/xwallpaper/"
  echo "make clean"
  echo "./configure"
  echo "make"
  echo "${ROOT_PRVL_CMD} ln -sf $(pwd)/xwallpaper /usr/bin/xwallpaper"
}

InstallingXSVI() {
  printMsg "xsvi installation"
  echo "cd ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/sxiv/"
  echo "make clean"
  echo "./configure"
  echo "make"
  echo "${ROOT_PRVL_CMD} ln -sf $(pwd)/sxiv /usr/bin/sxiv"
}

InstallingNvim() {
  printMsg "nvim installation"
  echo "mkdir ${HOME_CONFIG_PATH}/nvim"
  echo "cd ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/ides/text_based/nvim/"
  echo "cp -rf plugin_manager/nvim/ /home/$USER/.local/share/"

  echo "echo "cd ${MAIN_DOCUMENTS_PATH}/development/tools/""
  echo "tar -xf node-v14.15.4-linux-x64.tar.xz"
  echo "rm node-v14.15.4-linux-x64.tar.xz"

  echo "f=node; ${ROOT_PRVL_CMD} ln -sf $PWD/node-v14.15.4-linux-x64/bin/${f} /usr/bin/${f}"
  echo "f=npm; ${ROOT_PRVL_CMD} ln -sf $PWD/node-v14.15.4-linux-x64/bin/${f} /usr/bin/${f}"

  echo "echo "cd ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/appimages/""
  echo "chmod +x nvim-v044.appimage"
  echo "${ROOT_PRVL_CMD} ln -s  $(pwd)/nvim-v044.appimage /usr/bin/nvim"

  echo "ln ${HOME_CONFIG_PATH}/ides/text_based/nvim/init.vim /home/$USER/.config/nvim/"

  echo "mkdir /home/$USER/.vim/"
  echo "cp ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/ides/text_based/vim/plugins/nerd_tree.zip /home/$USER/.vim/"
  echo "cd /home/$USER/.vim/"
  echo "unzip nerd_tree.zip"
  echo "rm nerd_tree.zip"
  echo "cd"

  echo "echo "ln ${HOME_CONFIG_PATH}/ides/text_based/nvim/coc-settings.json  $HOME/.config/nvim/""
  echo "ln -sf ${MAIN_DOCUMENTS_PATH}/personal_stuff/apps/ides/text_based/colors/ /home/$USER/.vim/colors"
}

PreparingGitProjectDirectoryAndTheRepositories() {
  printMsg "Preparing GitProject directory and the repositories"
  echo "mkdir ${MAIN_DOCUMENTS_PATH}/development/gitprojects/"
  echo "cp -rfv ${harddrive_data_source}/environment/xorg_programs/{dwm,dmenu,st} ${MAIN_DOCUMENTS_PATH}/development/gitprojects/"
}

InstallingDWM() {
  printMsg "dwm installation"
  echo "cd ${MAIN_DOCUMENTS_PATH}/development/gitprojects/dwm/"
  echo "make clean && make && ${ROOT_PRVL_CMD} make install"
}

InstallingSt() {
  printMsg "st installation"
   echo "cd ${MAIN_DOCUMENTS_PATH}/development/gitprojects/st/"
   echo "make clean && make && ${ROOT_PRVL_CMD} make install"
}

InstallingDmenu() {
  printMsg "dmenu installation"
  echo "cd ${MAIN_DOCUMENTS_PATH}/development/gitprojects/dmenu/"
  echo "make clean && make && ${ROOT_PRVL_CMD} make install"
}

InstallingLy() {
  printMsg "ly installation"
  cp -rf ${harddrive_data_source}/environment/session_loaders/ly.tar ${MAIN_DOCUMENTS_PATH}/development/gitprojects/
  echo "cd ${MAIN_DOCUMENTS_PATH}/development/gitprojects/  "
      echo "tar -xf ly.tar                                     "
      echo "rm ly.tar                                          "
      echo "cd ly/ && make clean && make                       "
      echo "${ROOT_PRVL_CMD} make install && ${ROOT_PRVL_CMD} make installsystemd"
  echo "${ROOT_PRVL_CMD} cp ${harddrive_data_source}/environment/desktop_environment/dwl.desktop /usr/share/wayland-sessions/"
  #${ROOT_PRVL_CMD} cp ${harddrive_data_source}/environment/desktop_environment/dwm.desktop /usr/share/xsessions/
}

CopyingWallpapers() {
  printMsg "Copying wallpapers"
  echo "cp -rf ${harddrive_data_source}/wallpapers /home/$USER/"
}

RemovingAnnoyingUserDirectoryFolders() {
  printMsg "Removing Annoying User Directory Folders"
  for f in "Templates Videos Desktop Music Public Pictures"; do
    if [[ -d ${f} ]]; then
      echo "rm /home/$USER/${f}/ -rf"
    fi
  done
}

CheckDependencies() {
  echo "${MAG}-- The next flow of configurations would require your attention." $'\n' \
       "      -- Are you up to this or you'll walk away quickly ?${NC}"

  local options=(
    "yes" "#" # 1
  )
  declare -A menu=$(prepareMenu "${options[@]}")

  getUserSelection 0 "${options[@]}"
  declare user_selection=$?
  if [[ ${user_selection} -eq ${CANCEL_CODE} ]]; then
    exit 0
  fi

  LookingForBackupHarddrive

  if [[ ! -d ${harddrive_data_source} ]]; then
    echo $'\t' "-- Environment confg files, not found." $'\n' \
         $'\t' "-- External disk not mounted."
    exit 1
  fi

  deps="doas exfat-utils"
  for p in ${deps}; do
    e=$(whereis ${p})
    r=${e/${p}:/}
    [[ ! -v "r" ]] && echo "${p} is Not Installed" \
                   && echo "Bye ..." \
                   && exit 1
  done
}

# --------------- #
Main() {
  CheckDependencies
  CreatingMntDirectories
  InstallingDisplayServer
  InstallingSoundServer
  InstallingRemainingPrograms
  InstallingVirtManager
  InstallingYtdl
  CreatingCustomDirectories
  DownloadingConfigfilesRepo
  CopyingFromHarddrivePersonalstuff
  CopyingConfigFolderstoDotConfig
  MakingZathuraHardSymbolicLinksToConfigDir
  MakingPicomHardSymbolicLinkstoConfigDir
  MakingNeomuttHardSymbolicLinksToConfigDir
  MakingCalcurseHardSymbolicLinksToConfigDir
  MakingHardSymbolicLinksToUserDir
  CopyingFromHarddriveDevelopmentFolders
  InstallingCmake
  InstallingNvim
  PreparingGitProjectDirectoryAndTheRepositories
  if [[ ${display_server_chosen} == "xorg" ]]; then
    InstallingDWM
    InstallingSt
    InstallingDmenu
    InstallingXWallpaper
    InstallingXSVI
  fi
  InstallingLy
  CopyingWallpapers
  RemovingAnnoyingUserDirectoryFolders
}

# Run #
Main
