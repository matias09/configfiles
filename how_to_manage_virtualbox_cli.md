# Steps to set a new VM with VirtualBox CLI
**~ -> VBoxManage list ostypes  | grep Arch**
ID:          ArchLinux
Description: Arch Linux (32-bit)
ID:          ArchLinux_64
Description: Arch Linux (64-bit)

**~ -> VBoxManage createvm --name arch64 --ostype ArchLinux_64 --register**
Virtual machine 'arch64' is created and registered.
UUID: 4514cec8-fad1-4164-a240-8de08809edae
Settings file: '/home/matt/VirtualBox VMs/arch64/arch64.vbox'

**~ -> VBoxManage modifyvm arch64 --cpus 2 --memory 2048 --vram 12**
**~ -> VBoxManage modifyvm arch64 --nic1 bridged --bridgeadapter1 wlp3s0**

**~ -> VBoxManage createhd --filename VirtualBox\ VMs/arch64/arch64.vdi --size 10240 --variant Fixed**
0%...10%...20%...30%...40%...50%...60%...70%...80%...90%...100%
Medium created. UUID: 5292f77e-baa4-4601-9716-491b5d6e9883

**~ -> ls -lah VirtualBox\ VMs/arch64/arch64.vdi**
-rw-------. 1 matt matt 11G Feb 11 09:36 'VirtualBox VMs/arch64/arch64.vdi'

**~ -> VBoxManage storagectl arch64 --name "SATA Controller" --add sata --bootable on**

**~ -> VBoxManage storageattach arch64 --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium VirtualBox\ VMs/arch64/arch64.vdi**

**~ -> VBoxManage storagectl arch64 --name "IDE Controller" --add ide**

**~ -> VBoxManage storageattach arch64 --storagectl "IDE Controller" --port 0 --device 0 --type dvddrive --medium "/home/matt/Documents/development/os_images/archlinux-2020.07.03-dual.iso"**

**~ -> VBoxManage startvm arch64**
Waiting for VM "arch64" to power on...
VM "arch64" has been successfully started.
 ~ ->


------------------------------------------------------------------------------------------------

# Detach ISO, once SO is installed :)
**~ -> VBoxManage storageattach arch64 --storagectl "IDE Controller" --port 0 --device 0 --medium "none"**

# Full VM Deletion
**~ -> VBoxManage unregistervm --delete [vmname|uuid]
