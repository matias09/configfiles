#!/usr/bin bash


# ---------------------------------------------------------------------------
# Utilities functions
# ---------------------------------------------------------------------------

USE_CALLBACK=1
DONT_USE_CALLBACK=0

FAILURE_ERROR_CODE_TO_RETURN=-1
FAILURE_ERROR_CODE_RETURNED=255
CANCEL_CODE=0

printSeparator() {
  echo $'\n' \
  "${GRE}----------------------------------------------------------${NC}" \
  "${GRE}----------------------------------------------------------${NC}" $'\n'
}

printMsg() {
  printSeparator
  echo " ${MAG}[------- ${NC}${1}${MAG} -------]${NC}"
}

printWarningMsg() {
  printSeparator
  echo " ${RED}[------- ${NC}${1}${RED} -------]${NC}"
}

cancel() {
  echo " ${MAG}[------- ${NC}Operation Cancelled${MAG} -------]${NC}"
  return ${FAILURE_ERROR_CODE_TO_RETURN}
}

prepareMenu() {
  local options=("$@" "cancel" "#")

  currentOpt=""
  numberOpt=$((((${#options[@]} / 2))-1)) # start point from the max length "0"
  echo '('
  for strOpt in ${options[@]}; do
    if [[ ${strOpt} == "#" ]]; then
      echo "['${numberOpt}']='${currentOpt}'"
      let 'numberOpt=numberOpt - 1'
      currentOpt=""
      continue
    fi
    currentOpt="${currentOpt} ${strOpt}"
  done
  echo ')'
}
getUserSelection() {
  willUseCallbacks=${1}
  shift
  local menuOptions=("$@")
  declare -A menu=$(prepareMenu "${menuOptions[@]}")

  if [[ ${willUseCallbacks} -eq 1 ]]; then
    declare -A menuCallbacks
    for key in ${!menu[@]}; do
      menuCallbacks[${key}]=$(echo ${menu[${key}]} | sed 's/ //g')
    done
  fi

  echo "-- Options:"
  for key in ${!menu[@]}; do
    echo -n $'\t' " - [${key}] ${menu[${key}]}"
  done
  echo $'\n' "------------------------------"
  echo "-- Choose an option:"

  local user_selection=""
  valToReturn=""
  echo -n "--> " && read user_selection
  while :
  do
    if   [[ ${user_selection} -lt 0 ]] \
      || [[ -z ${menu[${user_selection}]} ]];
    then
      echo "Option Not Found . . ." $'\n'
      echo -n "--> " && read user_selection
      continue
    fi
    break
  done

  if [[ ${willUseCallbacks} -eq 1 ]]; then
    eval "${menuCallbacks[${user_selection}]}"
    valToReturn=$?
  else
    valToReturn=${user_selection}
  fi

  if [[ -z ${valToReturn} ]] || [[ ${valToReturn} == 0 ]]; then
    return 0
  fi
  return ${valToReturn}
}


# --------------------------------------------------------------------------
# Installation functions
# ---------------------------------------------------------------------------

CUSTOM_USER="matt"

InstallKernelAndUtilities() {
  printMsg "Install Kernel And Utilities"
  pacman -Syu --needed --noconfirm \
    linux linux-headers linux-lts linux-lts-headers \
    base-devel openssh dhcpcd grub grub-bios  \
    efibootmgr dosfstools os-prober mtools doas \
    wpa_supplicant wireless_tools networkmanager exfat-utils \
    linux-firmware doas vi git
}

SetLocationConf() {
  printMsg "Set Location Conf"
  export LANG="en_US.UTF-8 UTF-8"
  echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
  locale-gen
}

SetLocaltimeConf() {
  printMsg "Set Localtime Conf"
  ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
  hwclock --systohc --utc
}

EnableServices() {
  printMsg "Enable Services"
  systemctl enable dhcpcd
  systemctl enable NetworkManager
}

CreateRootPasswAndUserAndSetPassw() {
  printMsg "create_root_passw"
  passwd

  printMsg "create user ${CUSTOM_USER} and set passw"
  useradd -m -g users -G wheel ${CUSTOM_USER}
  passwd ${CUSTOM_USER}
}

RunGrubCmds() {
  printMsg "Run Grub Cmds"

  # Listing Devices
  lsblk

  declare user_selection=""
  echo -n $'\n'"-- Choose where to run grub-install ..."$'\n' \
               "--> " && read user_selection
  local device_name=${user_selection}

  local options=(
    "x86_64-efi" "#" # 1
    "i386-pc" "#" # 2
  )
  getUserSelection ${DONT_USE_CALLBACK} "${options[@]}"
  user_selection=$?
  if [[ ${user_selection} -eq ${CANCEL_CODE} ]]; then
    exit 0
  elif [[ ${user_selection} -eq 1 ]]; then
    printMsg "Installing x86_64-efi grub conf target"
    echo "grub-install --target=x86_64-efi --efi-directory=/boot \
            --bootloader-id=grub_uefi --recheck  /dev/${device_name}"
  else
    printMsg "Installing i386-pc grub conf target"
    echo "grub-install --target=i386-pc --recheck /dev/${device_name}"
  fi

  echo " mkdir /boot/grub/locale/"
  echo " cp /usr/share/locale/en@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo"
  echo " grub-mkconfig -o /boot/grub/grub.cfg"
}

InstallSecurityUcode() {
  printMsg "Install Security Ucode"

  local options=(
    "amd" "#"     # 1
    "intel" "#")  # 2

  getUserSelection ${DONT_USE_CALLBACK} "${options[@]}"
  declare user_selection=$?
  if [[ ${user_selection} -eq ${CANCEL_CODE} ]]; then
    exit 0
  elif [[ ${user_selection} -eq 1 ]]; then
    printMsg "Installing amd ucode"
    echo "pacman -S --needed --noconfirm amd-ucode"
  else
    printMsg "Installing intel ucode"
    echo "pacman -S --needed --noconfirm intel-ucode"
  fi
}

CreatingHomeFolders() {
  printMsg "Creating Home Folders"

  local CONFIGFILES_BITBUCKET_URL="https://bitbucket.org/matias09/configfiles"

  mkdir -p /home/${CUSTOM_USER}/{Documents,Downloads,.local/share/}
  chown -R ${CUSTOM_USER}:users /home/${CUSTOM_USER}/{Documents,Downloads,.local/}

  echo "-- Cloning configfiles"
  git clone ${CONFIGFILES_BITBUCKET_URL}

  printMsg "-- Moving configfiles"
  mv ./configfiles/ ${MAIN_DOCUMENTS_PATH}

  chown -R ${CUSTOM_USER}:users ${CONFIGFILES_PATH}
}

PreparingDesktopInitScriptDependencies() {
  printMsg "Preparing Desktop Init Script Dependencies"

  local MAIN_DOCUMENTS_PATH="/home/${CUSTOM_USER}/Documents"
  local CONFIGFILES_PATH="${MAIN_DOCUMENTS_PATH}/configfiles"
  local DESKTOP_ENVIRONMENT_DIR="${CONFIGFILES_PATH}/.matt_scripts/desktop_environment"
  local CUSTOM_SCRIPTS_DIR="/usr/share/custom_scripts"

  local DEPS=(${DESKTOP_ENVIRONMENT_DIR}/set_wallpapers.sh \
              ${DESKTOP_ENVIRONMENT_DIR}/auto_launch_program_scripts/init_wayland_env.sh \
              ${CONFIGFILES_PATH}/arch_linux/installation_script.sh \
              ${CONFIGFILES_PATH}/dot_config/kanshi)

  mkdir ${CUSTOM_SCRIPTS_DIR}

  for d in ${DEPS[@]}; do
    cp -rf ${d} ${CUSTOM_SCRIPTS_DIR}
  done

  cp -rf ${DESKTOP_ENVIRONMENT_DIR}/global_constants.sh /etc/profile.d/

  echo "alias runfirstenvscript=\"bash ${CONFIGFILES_PATH}/.matt_scripts/first_environment_setup.sh\"" >> /home/${CUSTOM_USER}/.bashrc
}


LastDetails() {
  printMsg "Last Details"

  echo "permit ${CUSTOM_USER} as root" > /etc/doas.conf
  printMsg "PLEASE COPY /etc/pacman.d/mirrorlist"
}

# --------------- #
Main() {
  InstallKernelAndUtilities
  SetLocationConf
  SetLocaltimeConf
  EnableServices
  CreateRootPasswAndUserAndSetPassw
# RunGrubCmds
# InstallSecurityUcode
# CreatingHomeFolders
# PreparingDesktopInitScriptDependencies
# LastDetails
}

# Run #
Main
