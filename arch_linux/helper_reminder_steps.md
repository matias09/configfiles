
-----------------------------------------------------------------------------

## Partition without LVM :
  The partitions are already created, formated and mounted
    /dev/sda1 /mnt
    /dev/sda2 /mnt/home

-----------------------------------------------------------------------------

## Steps to set Partition with LVM :
- fdisk /dev/[DEVICE]
-     create new partition layout [g]

-     create partition with [500]MB
-       set partition type to (EFI) [1]

-     create partition with [500]+MB
-       set partition type to (Linux Filesystem) [X]

-     create partition with [USER\_DESIRE]+GB
-       set partition type to (linux LVM) [30]

-     save changes [w]

##  Format Partition :
- mkfs.fat -F32 /dev/[FIRST\_PARTITION\_DEVICE]
- mkfs.ext4 /dev/[SECOND\_PARTITION\_DEVICE]

## Set encryption to third partition created :
- cryptsetup luksFormat /dev/[THIRD\_PARTITION\_DEVICE]
- cryptsetup open --type luks /dev/[THIRD\_PARTITION\_DEVICE] lvm

## Configurate LVM :
  **add --dataalignment 1m in case of NVND Harddrive Type**
- pvcreate /dev/mapper/lvm

- vgcreate volgroup0 /dev/mapper/lvm
- lvcreate -L 30GB volgroup0 -n lv\_root

  **Choose HALF AMOUNT OF THE Third Partition SPACE to use for LVM snapshots**
- lvcreate -L [HARDDRIVE / 2]GB volgroup0 -n lv\_home

-----------------------------------------------------------------------------
> To use the 100% of the free harddrive space use:
- lvcreate -l 100%FREE volgroup0 -n lv\_home
-----------------------------------------------------------------------------

## Format Volgroup Partition :
- mkfs.ext4 /dev/volgroup0/lv\_root
- mkfs.ext4 /dev/volgroup0/lv\_home

## Mount Volgroup Partition :
- mount /dev/volgroup0/lv\_root /mnt
mkdir /mnt/home
- mount /dev/volgroup0/lv\_home /mnt/home
mkdir /mnt/boot
- mount /dev/[SECOND\_PARTITION\_DEVICE] /mnt/boot
mkdir /mnt/etc

**----------------- Common Installation  Steps ---------------------------**

- genfstab -U -p /mnt >> /mnt/etc/fstab

- pacstrap -i /mnt base
- arch-chroot /mnt

### Another way to create a swapfile
  - fallocate -l 2GB /swapfile
  - chmod 600 /swapfile
  - mkswap    /swapfile
  - swapon    /swapfile
  - echo "/swapfile none swap sw 0 0" | tee -a  /etc/fstab

      just to be able to download a repo an execute a
        custom core installation script.
          - vi git

- pacman -S linux linux-headers linux-lts linux-lts-headers \
            base-devel openssh dhcpcd grub grub-bios  \
            efibootmgr dosfstools os-prober mtools \
            wpa_supplicant wireless_tools networkmanager vi git

**------------------ Steps for LVM SET UP --------------------------**
- pacman -S lvm2

- vim /etc/mkinitcpio.conf
-   search Regex __"^HOOKS"__
-   add between __"block" and "filesystems"__ "encrypt lvm2"
-   So result __"block encrypt lvm2 filesystems"__

- mkinitcpio -p linux
- mkinitcpio -p linux-lts

- vim /etc/default/grub
-   search Regex __"^#GRUB_ENABLE_CRYPTODISK"__ *----> uncomment that line*
-   search Regex __"^GRUB_CMDLINE_LINUX_DEFAULT"__
-   add between __"loglevel" and "quiet"__ "cryptdevice=/dev/[THIRD\_PARTITION\_DEVICE]:volgroup0:allow-discards"
-   So result "loglevel... cryptdevice=/dev/[THIRD\_PARTITION\_DEVICE]:volgroup0:allow-discards quiet"
   __check for no extra whitespaces between words__

- mkdir /boot/EFI
- mount /dev/[FIRST\_PARTITION\_DEVICE /boot/EFI
**------------------ END -----------------------------------------**

- export LANG="en_US.UTF-8"
- vi /etc/locale.gen
      ( en_US-UTF8...)
- locale-gen

- ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
- hwclock --systohc --utc

- systemctl enable dhcpcd
- systemctl enable NetworkManager

- passwd

- useradd -m -g users -G wheel matt
- passwd matt

**------------------ If LVM IS REQUIRED, SO --------------------------**
  - grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck  /dev/sda
**------------------ If Not Then -------------------------------------**
  - grub-install --target=i386-pc --recheck /dev/sda
**------------------ End If ------------------------------------------**

- mkdir /boot/grub/locale/
- cp /usr/share/locale/en\@quote/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo

- grub-mkconfig -o /boot/grub/grub.cfg

**------------------ Security Package Recomendation ------------------------**
  __amd version__
    - pacman -S amd-ucode
  __inter version__
    - pacman -S intel-ucode
**------------------ End ---------------------------------------------------**

**------------------ Video Driver Packages Recomendation -------------------**
  __amd version__
    - pacman -S mesa

  __nvidea__
   -- linux version
      - pacman -S nvidea nvidea-utils
   -- linux-lts version
      - pacman -S nvidea-lts  nvidea-utils
**------------------ End ---------------------------------------------------**

**------------------ VirtualBox Packages Recomendation ---------------------**
  - pacman -S virtualbox-guest-utils Xf86-video-vmware
**------------------ End ---------------------------------------------------**

**-------------->>>>>>>>>>> DONE <<<<<<<<<<<--------------------------------**
